package model;

import util.observer.Observable;

import javax.xml.stream.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

class TerritoryImpl extends Observable implements Territory, Serializable {
    private transient Robot robot;
    private int[][] tiles;
    private int robotDirection = ROBOT_DIRECTION_SOUTH;
    private int robotRow = 0;
    private int robotCol = 0;
    private boolean rescuing = false; // true, if the robot is carrying/rescuing a human
    private boolean rescueFinished = true; // as at start no human is placed
    private final transient Object robotInstanceLockObj = new Object();

    TerritoryImpl() {
        this(10, 10);
    }

    TerritoryImpl(int rows, int cols) {
        tiles = new int[rows][cols];
    }

    @Override
    public void placeRobot(int row, int col) throws TileBlockedException {
        synchronized (this) {
            if (tileOutOfBounds(row, col) || tiles[row][col] == TILE_FIRE)
                throw new TileBlockedException();
            robotRow = row;
            robotCol = col;
        }
        notifyObservers();
    }

    @Override
    public void placeHuman(int row, int col) throws TileBlockedException {
        synchronized (this) {
            if (tileOutOfBounds(row, col) || robotRow == row && robotCol == col)
                throw new TileBlockedException();
            tiles[row][col] = TILE_HUMAN;
            rescueFinished = false;
        }
        notifyObservers();
    }

    @Override
    public void placeRescueZone(int row, int col) throws TileBlockedException {
        synchronized (this) {
            if (tileOutOfBounds(row, col) || robotRow == row && robotCol == col)
                throw new TileBlockedException();
            tiles[row][col] = TILE_RESCUE_ZONE;
            checkRescueFinished();
        }
        notifyObservers();
    }

    @Override
    public void placeFire(int row, int col) throws TileBlockedException {
        synchronized (this) {
            if (tileOutOfBounds(row, col) || robotRow == row && robotCol == col)
                throw new TileBlockedException();
            tiles[row][col] = TILE_FIRE;
            checkRescueFinished();
        }
        notifyObservers();
    }

    @Override
    public void clearTile(int row, int col) throws TileBlockedException {
        synchronized (this) {
            if (tileOutOfBounds(row, col) || robotRow == row && robotCol == col)
                throw new TileBlockedException();
            tiles[row][col] = TILE_EMPTY;
            checkRescueFinished();
        }
        notifyObservers();
    }

    @Override
    public void robotMove() throws TileBlockedException {
        synchronized (this) {
            Tile next = nextTile();
            int row = next.getRow();
            int col = next.getCol();

            if (tileOutOfBounds(row, col) || tiles[row][col] == TILE_FIRE)
                throw new TileBlockedException();

            robotRow = row;
            robotCol = col;
        }
        notifyObservers();
    }

    @Override
    public void robotTurn() {
        synchronized (this) {
            robotDirection = robotDirection == ROBOT_DIRECTION_WEST ? ROBOT_DIRECTION_NORTH : robotDirection + 1;
        }
        notifyObservers();
    }

    @Override
    public void robotPickUp() throws IllegalTileException, RobotFullException {
        synchronized (this) {
            if (tiles[robotRow][robotCol] != TILE_HUMAN)
                throw new IllegalTileException();
            if (rescuing)
                throw new RobotFullException();

            rescuing = true;
            tiles[robotRow][robotCol] = TILE_EMPTY;
        }
        notifyObservers();
    }

    @Override
    public void robotDrop() throws TileBlockedException, RobotEmptyException {
        synchronized (this) {
            if (tiles[robotRow][robotCol] == TILE_HUMAN)
                throw new TileBlockedException();
            if (!rescuing) throw new RobotEmptyException();

            rescuing = false;
            tiles[robotRow][robotCol] = tiles[robotRow][robotCol] == TILE_RESCUE_ZONE ? TILE_RESCUE_ZONE : TILE_HUMAN;

            // Check if rescue is finished
            checkRescueFinished();
        }
        notifyObservers();
    }

    @Override
    public boolean robotWayBlocked() {
        synchronized (this) {
            Tile next = nextTile();
            int row = next.getRow();
            int col = next.getCol();
            return tileOutOfBounds(row, col) || tiles[row][col] == TILE_FIRE;
        }
    }

    @Override
    public boolean robotRescuing() {
        synchronized (this) {
            return rescuing;
        }
    }

    @Override
    public boolean robotHumanAhead() {
        synchronized (this) {
            Tile next = nextTile();
            int row = next.getRow();
            int col = next.getCol();
            return !tileOutOfBounds(row, col) && tiles[row][col] == TILE_HUMAN;
        }
    }

    @Override
    public boolean robotRescueZoneAhead() {
        synchronized (this) {
            Tile next = nextTile();
            int row = next.getRow();
            int col = next.getCol();
            return !tileOutOfBounds(row, col) && tiles[row][col] == TILE_RESCUE_ZONE;
        }
    }

    @Override
    public boolean robotRescueFinished() {
        synchronized (this) {
            return rescueFinished;
        }
    }

    @Override
    public int getTile(int row, int col) throws TileOutOfBoundsException {
        synchronized (this) {
            if(tileOutOfBounds(row, col))
                throw new TileOutOfBoundsException();
            return tiles[row][col];
        }
    }

    @Override
    public int getWidth() {
        synchronized (this) {
            return tiles[0].length;
        }
    }

    @Override
    public int getHeight() {
        synchronized (this) {
            return tiles.length;
        }
    }

    @Override
    public int getRobotRow() {
        synchronized (this) {
            return robotRow;
        }
    }

    @Override
    public int getRobotCol() {
        synchronized (this) {
            return robotCol;
        }
    }

    @Override
    public int getRobotDirection() {
        synchronized (this) {
            return robotDirection;
        }
    }

    @Override
    public void resize(int rows, int cols) {
        synchronized (this) {
            if(rows < 1 || cols < 1)
                throw new IllegalArgumentException("The width/height of the territory must be greater than zero!");
            if(rows == tiles.length && cols == tiles[0].length) return; // nothing to do

            int[][] newTiles = new int[rows][cols];

            if(rows > tiles.length && cols > tiles[0].length) {
                // Copy values
                for(int row = 0; row < tiles.length; row++) {
                    for(int col = 0; col < tiles[0].length; col++) {
                        newTiles[row][col] = tiles[row][col];
                    }
                }
            } else {
                boolean robotOutOfBounds = robotRow >= rows || robotCol >= cols;

                // Copy all possible tiles and replace robot if necessary
                for(int row = 0; row < tiles.length; row++) {
                    if(row >= rows) break;
                    for(int col = 0; col < tiles[0].length; col++) {
                        if(col >= cols) break;
                        if(robotOutOfBounds && tiles[row][col] != TILE_FIRE) {
                            robotRow = row;
                            robotCol = col;
                            robotOutOfBounds = false;
                        }
                        newTiles[row][col] = tiles[row][col];
                    }
                }

                // Robot still not placed?
                // Then the tile at pos (0, 0) must be a fire tile
                if(robotOutOfBounds) {
                    newTiles[0][0] = TILE_EMPTY;
                    robotRow = 0;
                    robotCol = 0;
                }
            }

            // Assign and check, if the rescue is now finished
            tiles = newTiles;
            checkRescueFinished();
        }
        notifyObservers();
    }

    @Override
    public void clear() {
        synchronized (this) {
            for(int row = 0; row < tiles.length; row++) {
                for(int col = 0; col < tiles[0].length; col++) {
                    tiles[row][col] = 0;
                }
            }
            checkRescueFinished();
        }
        notifyObservers();
    }

    @Override
    public void setRobotInstance(Robot robot) {
        synchronized (robotInstanceLockObj) {
            this.robot = robot;
        }
    }

    @Override
    public Robot getRobotInstance() {
        synchronized (robotInstanceLockObj) {
            return  robot;
        }
    }

    @Override
    public void serialize(String fileName) throws IOException {
        synchronized (this) {
            FileOutputStream fileOut = new FileOutputStream(fileName);
            ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
            objOut.writeObject(this);
            objOut.close();
            fileOut.close();
        }
    }

    @Override
    public void deserialize(String fileName) throws IOException, ClassNotFoundException {
        synchronized (this) {
            FileInputStream fileIn = new FileInputStream(fileName);
            ObjectInputStream objIn = new ObjectInputStream(fileIn);
            Territory ter = (Territory)objIn.readObject();

            // Start copying all values
            tiles = new int[ter.getHeight()][ter.getWidth()];
            for(int row = 0; row < tiles.length; row++)  {
                for(int col = 0; col < tiles[0].length; col++) {
                    tiles[row][col] = ter.getTile(row, col);
                }
            }
            robotDirection = ter.getRobotDirection();
            robotRow = ter.getRobotRow();
            robotCol = ter.getRobotCol();
            rescuing = ter.robotRescuing();
            rescueFinished = ter.robotRescueFinished();
        }
        notifyObservers();
    }

    @Override
    public synchronized String getAsXML() throws XMLStreamException {
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        StringWriter strWriter = new StringWriter();
        XMLStreamWriter writer = factory.createXMLStreamWriter(strWriter);
        writer.writeStartDocument();
        writer.writeCharacters("\n\n");
        writer.writeDTD("<!DOCTYPE territory [\n"
                + "<!ELEMENT territory (tile+)>\n"
                + "<!ATTLIST territory\n"
                + "width CDATA #REQUIRED\n"
                + "height CDATA #REQUIRED\n"
                + "robotDirection CDATA #REQUIRED\n"
                + "robotRow CDATA #REQUIRED\n"
                + "robotCol CDATA #REQUIRED\n"
                + "rescuing CDATA #REQUIRED>\n"
                + "<!ELEMENT tile EMPTY>\n"
                + "<!ATTLIST tile\n"
                + "row CDATA #REQUIRED\n"
                + "col CDATA #REQUIRED\n"
                + "type CDATA #REQUIRED>\n"
                + "]>");
        writer.writeCharacters("\n\n");
        writer.writeStartElement("territory");
        writer.writeAttribute("width", String.valueOf(tiles[0].length));
        writer.writeAttribute("height", String.valueOf(tiles.length));
        writer.writeAttribute("robotDirection", String.valueOf(robotDirection));
        writer.writeAttribute("robotRow", String.valueOf(robotRow));
        writer.writeAttribute("robotCol", String.valueOf(robotCol));
        writer.writeAttribute("rescuing", String.valueOf(rescuing));
        writer.writeCharacters("\n");

        for(int row = 0; row < tiles.length; row++) {
            for(int col = 0; col < tiles[0].length; col++) {
                if(tiles[row][col] == TILE_EMPTY) continue;
                writer.writeStartElement("tile");
                writer.writeAttribute("row", String.valueOf(row));
                writer.writeAttribute("col", String.valueOf(col));
                writer.writeAttribute("type", String.valueOf(tiles[row][col]));
                writer.writeEndElement();
                writer.writeCharacters("\n");
            }
        }

        writer.writeEndElement();
        writer.writeEndDocument();

        writer.flush();
        strWriter.flush();
        return strWriter.toString();
    }

    @Override
    public void fromXML(String xml) throws XMLStreamException {
        synchronized (this) {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            StringReader strReader = new StringReader(xml);
            XMLStreamReader parser = factory.createXMLStreamReader(strReader);
            while(parser.hasNext()) {
                switch (parser.getEventType()) {
                    case XMLStreamConstants.START_ELEMENT:
                        if(parser.getLocalName().equals("territory")) {
                            tiles = new int[Integer.parseInt(parser.getAttributeValue(0))]
                                    [Integer.parseInt(parser.getAttributeValue(1))];
                            robotDirection = Integer.parseInt(parser.getAttributeValue(2));
                            robotRow = Integer.parseInt(parser.getAttributeValue(3));
                            robotCol = Integer.parseInt(parser.getAttributeValue(4));
                            rescuing = Boolean.parseBoolean(parser.getAttributeValue(5));
                        } else { // tile
                            tiles[Integer.parseInt(parser.getAttributeValue(0))]
                                    [Integer.parseInt(parser.getAttributeValue(1))]
                                    = Integer.parseInt(parser.getAttributeValue(2));
                        }
                        break;
                    default:
                        // ignore
                        break;
                }
                parser.next();
            }
            checkRescueFinished();
        }
        notifyObservers();
    }

    @Override
    public void saveXML(String fileName) throws IOException, XMLStreamException {
        FileOutputStream fileOut = new FileOutputStream(fileName);
        PrintStream ps = new PrintStream(fileOut);
        ps.print(getAsXML());
        ps.close();
        fileOut.close();
    }

    @Override
    public void loadXML(String fileName) throws IOException, XMLStreamException {
        fromXML(new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8));
    }


    private boolean tileOutOfBounds(int row, int col) {
        return row < 0 || col < 0 || row >= tiles.length || col >= tiles[0].length;
    }

    /**
     * Gibt die Position zurück, in der sich der Roboter in der aktuellen Position als nächstes bewegen würde,
     * wenn der move()-Command ausgeführt wird.
     *
     * @return Die nächste Kachel
     */
    private Tile nextTile() {
        switch (robotDirection) {
            case ROBOT_DIRECTION_NORTH:
                return Tile.newInstance(robotRow - 1, robotCol);
            case ROBOT_DIRECTION_EAST:
                return Tile.newInstance(robotRow, robotCol + 1);
            case ROBOT_DIRECTION_SOUTH:
                return Tile.newInstance(robotRow + 1, robotCol);
            case ROBOT_DIRECTION_WEST:
                return Tile.newInstance(robotRow, robotCol - 1);
            default:
                throw new IllegalStateException("CRITICAL ERROR: The Robot is in an illegal state." +
                        "Error Code: 0x01 - Robot direction unspecified. Please report this bug immediately.");
        }
    }

    private void checkRescueFinished() {
        boolean humanPresent = false;
        for (int row = 0; row < tiles.length; row++) {
            for (int col = 0; col < tiles[0].length; col++) {
                if (tiles[row][col] == TILE_HUMAN) humanPresent = true;
            }
        }
        rescueFinished = !humanPresent && !rescuing;
    }

    /*
    *---------------------------------------------------------------
    * Old temporary test methods
    *---------------------------------------------------------------
     */
    private void print() {
        System.out.printf("Rescucing Human: %b\nRescue finished: %b\n", rescuing, rescueFinished);
        for(int i = 0; i < tiles[0].length; i++) {
            if(i == 0) System.out.printf("  %d ", i);
            else if(i == tiles[0].length - 1) System.out.printf("%d\n", i);
            else System.out.printf("%d ", i);
        }
        for(int row = 0; row < tiles.length; row++) {
            System.out.printf("%d ", row);
            for(int col = 0; col < tiles[0].length; col++) {
                if(row == robotRow && col == robotCol && robotDirection == ROBOT_DIRECTION_NORTH) {
                    System.out.printf("∧ ");
                    continue;
                }
                else if(row == robotRow && col == robotCol && robotDirection == ROBOT_DIRECTION_EAST) {
                    System.out.printf("> ");
                    continue;
                }
                else if(row == robotRow && col == robotCol && robotDirection == ROBOT_DIRECTION_WEST) {
                    System.out.printf("< ");
                    continue;
                }
                else if(row == robotRow && col == robotCol && robotDirection == ROBOT_DIRECTION_SOUTH) {
                    System.out.printf("v ");
                    continue;
                }

                if(tiles[row][col] == TILE_FIRE) System.out.printf("f ");
                else if(tiles[row][col] == TILE_EMPTY) System.out.printf("- ");
                else if(tiles[row][col] == TILE_HUMAN) System.out.printf("H ");
                else if(tiles[row][col] == TILE_RESCUE_ZONE) System.out.printf("R ");
            }
            System.out.printf("\n");
        }

        System.out.printf("\n\n");
    }

    // Temporäre Testmethode
    public static void main(String[] args) throws Exception {
        TerritoryImpl territory = new TerritoryImpl();
        territory.placeFire(3, 3);
        territory.placeHuman(2, 2);
        territory.placeRescueZone(1, 1);
        territory.placeRobot(2, 2);
        territory.robotPickUp();
        territory.placeFire(2, 3);
        territory.print();
        territory.robotMove();
        territory.robotMove();
        territory.robotTurn();
        territory.robotTurn();
        territory.robotTurn();
        territory.print();

        // Test store methods
        territory.saveXML("territories/test.xml");
        TerritoryImpl t2 = new TerritoryImpl();
        t2.loadXML("territories/test.xml");
        t2.print();
    }
}
