package model;

class ExampleImpl implements Example {
    private final String name;
    private final String code;
    private final String territory;

    ExampleImpl(String name, String code, String territory) {
        this.name = name;
        this.code = code;
        this.territory = territory;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getTerritory() {
        return territory;
    }
}