package model;

public class RobotImpl implements Robot{
    private Territory territory; // Set via reflection

    @Override
    public void main() {

    }

    @Override
    public void move()  {
        territory.robotMove();
    }

    @Override
    public void turnRight() {
        territory.robotTurn();
    }

    @Override
    public void pickUp() {
        territory.robotPickUp();
    }

    @Override
    public void drop() {
        territory.robotDrop();
    }

    @Override
    public boolean wayBlocked() {
        return territory.robotWayBlocked();
    }

    @Override
    public boolean rescuing() {
        return territory.robotRescuing();
    }

    @Override
    public boolean humanAhead() {
        return territory.robotHumanAhead();
    }

    @Override
    public boolean rescueZoneAhead() {
        return territory.robotRescueZoneAhead();
    }

    @Override
    public boolean rescueFinished() {
        return territory.robotRescueFinished();
    }
}