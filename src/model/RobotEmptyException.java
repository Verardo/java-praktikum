package model;

class RobotEmptyException extends RuntimeException {
    RobotEmptyException() {
        super("The Robot is empty! Nothing can be dropped.");
    }
}