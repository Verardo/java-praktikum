package model;

public interface Tile {
    int getRow();
    int getCol();
    static Tile newInstance(int row, int col) {
        return new TileImpl(row, col);
    }
}
