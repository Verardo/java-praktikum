package model;

public interface Robot {
    void main();
    void move() throws TileBlockedException;
    void turnRight();
    void pickUp();
    void drop();
    boolean wayBlocked();
    boolean rescuing();
    boolean humanAhead();
    boolean rescueZoneAhead();
    boolean rescueFinished();
}