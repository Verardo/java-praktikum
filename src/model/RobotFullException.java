package model;

class RobotFullException extends RuntimeException {
    RobotFullException() {
        super("The robot is at max capacity! No more humans can be carried.");
    }
}