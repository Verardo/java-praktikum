package model;

class TileImpl implements Tile {
    private int row;
    private int col;

    TileImpl(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public int getRow() {
        return row;
    }

    @Override
    public int getCol() {
        return col;
    }
}
