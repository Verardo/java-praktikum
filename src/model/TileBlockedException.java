package model;

class TileBlockedException extends RuntimeException {
    TileBlockedException() {
        super("The Robot could not move on this tile");
    }
}