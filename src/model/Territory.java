package model;

import util.observer.Observer;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public interface Territory {
    // Robot directions
    int ROBOT_DIRECTION_NORTH = 0;
    int ROBOT_DIRECTION_EAST = 1;
    int ROBOT_DIRECTION_SOUTH = 2;
    int ROBOT_DIRECTION_WEST = 3;

    // Tiles
    int TILE_FIRE = -1;
    int TILE_EMPTY = 0;
    int TILE_HUMAN = 1;
    int TILE_RESCUE_ZONE = 2;

    // Place something on or clear tile
    void placeRobot(int row, int col) throws TileBlockedException;
    void placeHuman(int row, int col) throws TileBlockedException;
    void placeRescueZone(int row, int col) throws TileBlockedException;
    void placeFire(int row, int col) throws TileBlockedException;
    void clearTile(int row, int col) throws TileBlockedException;

    // Robot actions
    void robotMove() throws TileBlockedException;
    void robotTurn();
    void robotPickUp() throws IllegalTileException, RobotFullException;
    void robotDrop() throws TileBlockedException, RobotEmptyException;

    // Robot "questions"
    boolean robotWayBlocked();
    boolean robotRescuing();
    boolean robotHumanAhead();
    boolean robotRescueZoneAhead();
    boolean robotRescueFinished();

    // Internal getters
    int getTile(int row, int col) throws TileOutOfBoundsException;
    int getWidth();
    int getHeight();
    int getRobotRow();
    int getRobotCol();
    int getRobotDirection();

    // Other
    void resize(int rows, int cols);
    void clear();
    void addObserver(Observer obs);
    void deleteObserver(Observer obs);
    void setRobotInstance(Robot robot);
    Robot getRobotInstance();

    // Store
    void serialize(String fileName) throws IOException;
    void deserialize(String fileName) throws IOException, ClassNotFoundException;
    String getAsXML() throws XMLStreamException;
    void fromXML(String xml) throws XMLStreamException;
    void saveXML(String fileName) throws IOException, XMLStreamException;
    void loadXML(String fileName) throws IOException, XMLStreamException;

    static Territory newInstance() {
        return new TerritoryImpl();
    }

    static Territory newInstance(int rows, int cols) {
        return new TerritoryImpl(rows, cols);
    }
}