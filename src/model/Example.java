package model;

public interface Example {
    String getName();
    String getCode();
    String getTerritory();
    static Example newInstance(String name, String code, String territory) {
        return new ExampleImpl(name, code, territory);
    }
}