package controller.simulation;

import javafx.application.Platform;
import model.Robot;
import model.Territory;
import util.observer.Observable;
import util.observer.Observer;
import view.notification.Notification;

class SimulationImpl extends Thread implements Simulation, Observer {
    private final Territory territory;
    private final SimulationManager simMgr;
    private final Object lockObj;
    private Robot robot;
    private boolean paused = false;

    SimulationImpl(Territory territory, SimulationManager simMgr,  Object lockObj) {
        this.territory = territory;
        this.simMgr = simMgr;
        this.lockObj = lockObj;
    }

    @Override
    public void run() {
        territory.addObserver(this);
        robot = territory.getRobotInstance();
        while(true) {
            try {
                robot.main();
            } catch (RobotChangedException ex) {
                continue;
            } catch (Exception ex) {
                if(!(ex instanceof SimulationInterruptedException))
                    Platform.runLater(() ->
                        Notification.showErrorNotification(
                                "Error during Simulation: " + ex.getClass().getSimpleName().replace("Exception", ""), ex.getMessage()));
            }
            break;
        }
        territory.deleteObserver(this);
        simMgr.simulationEnded();
        interrupt();
    }

    @Override
    public void update(Observable o, Object arg) {
        if(paused) return;
        try{
            Thread.sleep(1010 - simMgr.getSpeed() * 100);
            if(simMgr.getState() == SimulationState.PAUSED) {
                synchronized (lockObj) {
                    paused = true;
                    lockObj.wait();
                    paused = false;
                }
            }
            Robot r = territory.getRobotInstance();
            if(r != robot) {
                robot = r;
                throw new RobotChangedException();
            }
        } catch (Exception ex) {
            if(ex instanceof RobotChangedException) throw new RobotChangedException();
            throw new SimulationInterruptedException();
        }
    }
}