package controller.simulation;

import model.Territory;

public interface Simulation {
    void start();
    void interrupt();
    static Simulation newInstance(Territory territory, SimulationManager simMgr, Object lockObj) {
        return new SimulationImpl(territory, simMgr, lockObj);
    }
}
