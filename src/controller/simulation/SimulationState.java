package controller.simulation;

public enum SimulationState {
    RUNNING,
    PAUSED,
    NOT_RUNNING
}
