package controller.simulation;

import model.Territory;

import java.util.Observer;

public interface SimulationManager {
    void run(Territory territory);
    void pause();
    void stop();
    void simulationEnded();
    SimulationState getState();
    void setSpeed(int speed);
    int getSpeed();
    void addObserver(Observer obs);
    static SimulationManager newInstance(Territory territory) {
        return new SimulationManagerImpl(territory);
    }
}
