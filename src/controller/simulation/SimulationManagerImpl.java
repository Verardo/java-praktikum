package controller.simulation;

import model.Territory;
import view.notification.Notification;

import java.util.Observable;

class SimulationManagerImpl extends Observable implements SimulationManager {
    private SimulationState state = SimulationState.NOT_RUNNING;
    private volatile int speed = 5;
    private final Object lockObj = new Object();
    private Simulation simulation;

    SimulationManagerImpl(Territory territory) {
        simulation = Simulation.newInstance(territory, this, lockObj);
    }

    @Override
    public void run(Territory territory) {
        // Check if the territory has a valid robot instance (not null)
        // (Situation: After restart of the Application with a faulty program, the automatic compilation
        // will fail and thus no robot instance will be set in the territory)
        if(territory.getRobotInstance() == null) {
            Notification.showErrorNotification("Robot not compiled!",
                    "The Robot needs to be compiled, before a Simulation can be started!");
            simulationEnded();
            return;
        }
        synchronized (lockObj) {
            if(state == SimulationState.PAUSED) {
                state = SimulationState.RUNNING;
                lockObj.notify();
            } else {
                simulation = Simulation.newInstance(territory, this, lockObj);
                state = SimulationState.RUNNING;
                simulation.start();
            }
        }
    }

    @Override
    public void pause() {
        synchronized (lockObj) {
            state = SimulationState.PAUSED;
        }
    }

    @Override
    public void stop() {
        synchronized (lockObj) {
            simulation.interrupt();
            state = SimulationState.NOT_RUNNING;
            lockObj.notify();
        }
    }

    @Override
    public void simulationEnded() {
        synchronized (lockObj) {
            state = SimulationState.NOT_RUNNING;
            setChanged();
            notifyObservers();
        }
    }

    @Override
    public SimulationState getState() {
        synchronized (lockObj) {
            return state;
        }
    }

    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public int getSpeed() {
        return speed;
    }
}
