package controller.editor;

public interface EditorFileNewController {
    static EditorFileNewController newInstance() {
        return new EditorFileNewControllerImpl();
    }
}
