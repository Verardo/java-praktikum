package controller.editor;

public interface EditorFileOpenController {
    static EditorFileOpenController newInstance() {
        return new EditorFileOpenControllerImpl();
    }
}
