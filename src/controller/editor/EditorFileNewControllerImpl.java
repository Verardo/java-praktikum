package controller.editor;

import controller.program.ProgramManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import util.validator.Validator;
import view.dialog.Dialog;

import java.util.Observable;
import java.util.Observer;

class EditorFileNewControllerImpl implements EditorFileNewController, EventHandler<ActionEvent>, Observer {
    @Override
    public void handle(ActionEvent event) {
        Dialog dialog = Dialog.newSingleInputDialog("New program", "Create new program",
                "Program name:", "program name");
        dialog.setValidator(Validator.createValidatorJavaFile());
        dialog.addObserver(this);
        dialog.showInputDialog();
    }

    @Override
    public void update(Observable o, Object arg) {
        String str = (String)arg;
        ProgramManager.run(str.endsWith(".java") ? str : str.concat(".java"));
    }
}
