package controller.editor;

import controller.program.ProgramManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

class EditorFileOpenControllerImpl implements EditorFileOpenController, EventHandler<ActionEvent> {
    @Override
    public void handle(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File(ProgramManager.FOLDER));
        chooser.setTitle("Open .java program");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Java files (*.java)", "*.java"));
        File file = chooser.showOpenDialog(new Stage());
        if(file != null)
            ProgramManager.run(file.getName());
    }
}
