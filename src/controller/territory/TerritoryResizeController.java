package controller.territory;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import model.Territory;

public interface TerritoryResizeController extends EventHandler<ActionEvent> {
    static TerritoryResizeController newInstance(Territory territory) {
        return new TerritoryResizeControllerImpl(territory);
    }
}