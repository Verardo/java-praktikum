package controller.territory;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Pair;
import model.Territory;
import util.validator.Validator;
import view.dialog.Dialog;

import java.util.Observable;
import java.util.Observer;

class TerritoryResizeControllerImpl implements TerritoryResizeController, EventHandler<ActionEvent>, Observer {
    private final Territory territory;

    TerritoryResizeControllerImpl(Territory territory) {
        this.territory = territory;
    }

    @Override
    public void handle(ActionEvent event) {
        Dialog dialog = Dialog.newDoubleInputDialog("Resize Territory", "Resize Territory",
                "Rows (1 to 100):", "Columns (1 to 100):", "Rows", "Cols",
                String.valueOf(territory.getHeight()), String.valueOf(territory.getWidth()));
        dialog.setValidator(Validator.createValidatorTerritoryResize());
        dialog.addObserver(this);
        dialog.showInputDialog();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Observable o, Object arg) {
        Pair<String, String> pair = (Pair<String, String>)arg;
        territory.resize(Integer.parseInt(pair.getKey()), Integer.parseInt(pair.getValue()));
    }
}
