package controller.territory;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import view.page.DefaultPage;
import view.page.component.bar.ToggleButtonType;

import java.util.HashMap;
import java.util.Map;

class TerritoryPanelCursorControllerImpl implements TerritoryPanelCursorController, EventHandler<MouseEvent> {
    private final DefaultPage page;
    private final Map<ToggleButtonType, Cursor> cursors = new HashMap<>();


    TerritoryPanelCursorControllerImpl(DefaultPage page) {
        this.page = page;

        // Fill cursors map
        BorderPane pane = (BorderPane)page;
        cursors.put(ToggleButtonType.ROBOT, Cursor.DEFAULT); // Default cursor for robot
        cursors.put(ToggleButtonType.HUMAN, new ImageCursor(new Image(getClass().getResourceAsStream("/res/bar/human.png"))));
        cursors.put(ToggleButtonType.RESCUE_ZONE, new ImageCursor(new Image(getClass().getResourceAsStream("/res/bar/rescue_zone.png"))));
        cursors.put(ToggleButtonType.FIRE, new ImageCursor(new Image(getClass().getResourceAsStream("/res/bar/fire.png"))));
        cursors.put(ToggleButtonType.CLEAR_TILE, new ImageCursor(new Image(getClass().getResourceAsStream("/res/bar/clear.png"))));
    }

    @Override
    public void handle(MouseEvent event) {
        if(event.getEventType().getName().equals("MOUSE_ENTERED")) {
            ((Scene)page.getRootObject()).setCursor(cursors.get(page.getBar().selectedToggleButton()));
        } else {
            ((Scene)page.getRootObject()).setCursor(Cursor.DEFAULT);
        }
    }
}