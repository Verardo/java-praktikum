package controller.territory;

import controller.program.Invisible;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.media.AudioClip;
import model.Robot;
import model.Territory;
import model.Tile;
import view.notification.Notification;
import view.page.component.bar.Bar;
import view.page.component.territory.TerritoryPanel;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.List;

class TerritoryPanelControllerImpl implements TerritoryPanelController, EventHandler {
    private final Territory territory;
    private final TerritoryPanel territoryPanel;
    private final Bar bar;
    private final ContextMenu menu = new ContextMenu();
    private Robot robot;
    private final AudioClip clip = new AudioClip(new File("src/res/sound/error.wav").toURI().toString());

    TerritoryPanelControllerImpl(Territory territory, TerritoryPanel territoryPanel, Bar bar) {
        this.territory = territory;
        this.territoryPanel = territoryPanel;
        this.bar = bar;
    }

    @Override
    public void handle(Event event) {
        if(event instanceof ContextMenuEvent) {
            ContextMenuEvent cme = (ContextMenuEvent)event;

            // Check: Was the click performed upon the Robot?
            Tile tile = territoryPanel.getTileFromMousePosition((int)cme.getX(), (int)cme.getY());
            if(!(territory.getRobotRow() == tile.getRow()) || !(territory.getRobotCol() == tile.getCol()))
                return;

            // Check: Is reloading necessary?
            if(territory.getRobotInstance() != robot) {
                robot = territory.getRobotInstance();
                menu.getItems().clear();
                fillMenu();
            }

            // Show menu if not already showing
            if(!menu.isShowing()) menu.show((Region)territoryPanel, cme.getScreenX(), cme.getScreenY());

        } else if(event instanceof MouseEvent && !((MouseEvent) event).isSecondaryButtonDown()) {
            MouseEvent me = (MouseEvent)event;

            // Check: menu showing? If yes, then the next click is supposed to close the menu and not
            // place any tiles on the territory
            if(menu.isShowing()) {
                menu.hide();
                return;
            }

            // Get tile
            Tile tile = territoryPanel.getTileFromMousePosition((int)me.getX(), (int)me.getY());

            // Perform action
            try{
                switch (bar.selectedToggleButton()) {
                    case ROBOT:
                        territory.placeRobot(tile.getRow(), tile.getCol());
                        break;
                    case HUMAN:
                        territory.placeHuman(tile.getRow(), tile.getCol());
                        break;
                    case RESCUE_ZONE:
                        territory.placeRescueZone(tile.getRow(), tile.getCol());
                        break;
                    case FIRE:
                        territory.placeFire(tile.getRow(), tile.getCol());
                        break;
                    case CLEAR_TILE:
                        territory.clearTile(tile.getRow(), tile.getCol());
                        break;
                }
            } catch (Exception ex) {
                // ignore exception, as the action just needs to be interrupted
            }
        }
    }

    private void fillMenu() {
        // Get MenuItem List reference
        List<MenuItem> items = menu.getItems();

        // Disable marks all MenuItems, that have to be disabled after creation
        boolean disable = false;

        // Get all methods declared in the new robot class
        mainLoop:
        for(Method m : territory.getRobotInstance().getClass().getDeclaredMethods()) {
            String methodName = m.getName();

            // Check if method can be ignored
            if(m.getModifiers() == Modifier.ABSTRACT) continue;
            else if(m.getModifiers() == Modifier.STATIC) continue;
            else if(m.getModifiers() == Modifier.PRIVATE) continue;

            // Check if method is annotated with "@Invisible"
            Annotation[] annotations = m.getAnnotations();
            for(Annotation a : annotations) if(a.annotationType() == Invisible.class) continue mainLoop;

            // Initialize StringBuilder for output name
            StringBuilder name = new StringBuilder();

            // Return type?
            if(m.getReturnType() != null) {
                String returnType = m.getReturnType().getSimpleName();
                if(!returnType.equals("void")) {
                    name.append(returnType);
                    name.append(" ");
                }
            }

            name.append(methodName);
            name.append("(");

            // Append parameters and ")"
            Parameter[] params = m.getParameters();

            // Has parameters?
            if(params.length > 0) {
                // Append all parameters
                for(int i = 0; i < params.length; i++) {
                    if(i != 0) name.append(", ");
                    name.append(params[i].getType().getSimpleName());
                }
                disable = true;
            }

            // Create item
            name.append(")");
            MenuItem item = new MenuItem(name.toString());

            // Set action
            item.setOnAction(e -> {
                if(!m.getReturnType().getSimpleName().equals("void") && (m.getParameters() == null || m.getParameters().length < 1)) {
                    try{
                        m.setAccessible(true);
                        new Alert(Alert.AlertType.INFORMATION, m.invoke(robot).toString(), ButtonType.OK).showAndWait();
                        m.setAccessible(false);
                    } catch(Exception ex) {
                        ex.printStackTrace();
                    }
                } else if(m.getParameters() == null || m.getParameters().length < 1){
                    try{
                        m.setAccessible(true);
                        m.invoke(robot);
                        m.setAccessible(false);
                    } catch(IllegalAccessException ex) {
                        ex.printStackTrace();
                    } catch (InvocationTargetException ex) {
                        clip.play();
                    }
                }
            });

            if(disable) {
                item.setDisable(true);
                disable = false;
            }

            // Add item to ContextMenu
            items.add(item);
        }

        // Seperate
        items.add(new SeparatorMenuItem());

        // Get all basic methods from the superclass
        for(Method m : territory.getRobotInstance().getClass().getSuperclass().getDeclaredMethods()) {
            String methodName = m.getName();

            // Check if method can be ignored
            if(m.getModifiers() != Modifier.PUBLIC) continue;
            else if(methodName.equals("setTerritory")) continue; // helper method
            else if(methodName.equals("main")) continue; // Must be overriden by method

            // Initialize StringBuilder for output name
            StringBuilder name = new StringBuilder();

            // Return type?
            if(m.getReturnType() != null) {
                String returnType = m.getReturnType().getSimpleName();
                if(!returnType.equals("void")) {
                    name.append(returnType);
                    name.append(" ");
                }
            }

            name.append(methodName);
            name.append("(");

            // Append parameters and ")"
            Parameter[] params = m.getParameters();

            // Has parameters?
            if(params.length > 0) {
                // Append all parameters
                for(int i = 0; i < params.length; i++) {
                    if(i != 0) name.append(", ");
                    name.append(params[i].getType().getSimpleName());
                }
                disable = true;
            }

            // Create item
            name.append(")");
            MenuItem item = new MenuItem(name.toString());

            // Set action
            item.setOnAction(e -> {
                if(!m.getReturnType().getSimpleName().equals("void") && (m.getParameters() == null || m.getParameters().length < 1)) {
                    try{
                        m.setAccessible(true);
                        Notification.showInfoNotification("Value information: " + m.invoke(robot).toString(),
                                "The method \"" + item.getText() + "\" returned the value above");
                        m.setAccessible(false);
                    } catch(Exception ex) {
                        ex.printStackTrace();
                    }
                } else if(m.getParameters() == null || m.getParameters().length < 1){
                    try{
                        m.setAccessible(true);
                        m.invoke(robot);
                        m.setAccessible(false);
                    } catch(IllegalAccessException ex) {
                        ex.printStackTrace();
                    } catch (InvocationTargetException ex) {
                        clip.play();
                    }
                }
            });

            if(disable) {
                item.setDisable(true);
                disable = false;
            }

            // Add item to ContextMenu
            items.add(item);
        }
    }
}
