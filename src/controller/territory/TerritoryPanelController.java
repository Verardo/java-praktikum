package controller.territory;

import model.Territory;
import view.page.component.bar.Bar;
import view.page.component.territory.TerritoryPanel;

public interface TerritoryPanelController {
    static TerritoryPanelController newInstance(Territory territory, TerritoryPanel territoryPanel, Bar bar) {
        return new TerritoryPanelControllerImpl(territory, territoryPanel, bar);
    }
}