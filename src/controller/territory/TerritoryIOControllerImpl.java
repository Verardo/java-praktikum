package controller.territory;

import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Territory;
import view.notification.Notification;

import java.io.File;

class TerritoryIOControllerImpl implements TerritoryIOController {
    private final Territory territory;
    private final File territoryDir = new File("territories");

    TerritoryIOControllerImpl(Territory territory) {
        this.territory = territory;
        createTerritoryDirectoryIfNecessary();
    }

    @Override
    public void saveSerialize() {
        FileChooser chooser = prepFileChooser();
        chooser.setTitle("Save Territory to .territory");
        chooser.getExtensionFilters().add(getTerritoryExtensionFilter());
        File result = chooser.showSaveDialog(new Stage());
        if(result != null) {
            try {
                territory.serialize(territoryDir + File.separator + withDotTerritoryExtension(result.getName()));
            } catch (Exception ex) {
                Notification.showErrorNotification("Territory could not be saved!",
                        "There was an error while trying to save the territory (Serialize).\n"
                                + ex.toString());
            }
        }
    }

    @Override
    public void loadDeserialize() {
        FileChooser chooser = prepFileChooser();
        chooser.setTitle("Load Territory from .territory");
        chooser.getExtensionFilters().add(getTerritoryExtensionFilter());
        File result = chooser.showOpenDialog(new Stage());
        if(result != null) {
            try {
                territory.deserialize(territoryDir + File.separator + withDotTerritoryExtension(result.getName()));
            } catch (Exception ex) {
                Notification.showErrorNotification("Territory could not be loaded!",
                        "There was an error while trying to load the territory (Deserialize).\n"
                                + ex.toString());
            }
        }
    }

    @Override
    public void saveXML() {
        FileChooser chooser = prepFileChooser();
        chooser.setTitle("Save Territory to .xml");
        chooser.getExtensionFilters().add(getXMLExtensionFilter());
        File result = chooser.showSaveDialog(new Stage());
        if(result != null) {
            try{
                territory.saveXML(territoryDir + File.separator + withDotXMLExtension(result.getName()));
            } catch (Exception ex) {
                Notification.showErrorNotification("Territory could not be saved!",
                        "There was an error while trying to save the territory (XML).\n"
                                + ex.toString());
            }
        }
    }

    @Override
    public void loadXML() {
        FileChooser chooser = prepFileChooser();
        chooser.setTitle("Load Territory from .xml");
        chooser.getExtensionFilters().add(getXMLExtensionFilter());
        File result = chooser.showOpenDialog(new Stage());
        if(result != null) {
            try{
                territory.loadXML(territoryDir + File.separator + withDotXMLExtension(result.getName()));
            } catch (Exception ex) {
                Notification.showErrorNotification("Territory could not be loaded!",
                        "There was an error while trying to load the territory (XML).\n"
                                + ex.toString());
            }
        }
    }

    private void createTerritoryDirectoryIfNecessary() {
        if(!territoryDir.exists()) {
            boolean result = territoryDir.mkdir();
            if(!result) {
                Notification.showErrorNotification("Error creating \"territories\" directory!",
                        "The directory \"territories\" could not be created. Do you have" +
                                "writing permissions for the current directory?");
            }
        }
    }

    private FileChooser prepFileChooser() {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(territoryDir);
        return chooser;
    }

    private String withDotTerritoryExtension(String str) {
        return str.endsWith(".territory") ? str : str.concat(".territory");
    }

    private FileChooser.ExtensionFilter getTerritoryExtensionFilter() {
        return new FileChooser.ExtensionFilter("Territory file (.territory)", "*.territory");
    }

    private FileChooser.ExtensionFilter getXMLExtensionFilter() {
        return new FileChooser.ExtensionFilter("Territory XML file (.xml)", "*.xml");
    }

    private String withDotXMLExtension(String str) {
        return str.endsWith(".xml") ? str : str.concat(".xml");
    }
}