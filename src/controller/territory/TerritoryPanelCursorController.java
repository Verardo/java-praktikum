package controller.territory;

import view.page.DefaultPage;

public interface TerritoryPanelCursorController {
    static TerritoryPanelCursorController newInstance(DefaultPage page) {
        return new TerritoryPanelCursorControllerImpl(page);
    }
}