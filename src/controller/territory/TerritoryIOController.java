package controller.territory;

import model.Territory;

public interface TerritoryIOController {
    void saveSerialize();
    void loadDeserialize();
    void saveXML();
    void loadXML();
    static TerritoryIOController newInstance(Territory territory) {
        return new TerritoryIOControllerImpl(territory);
    }
}