package controller.tutor;

import model.Territory;
import view.page.DefaultPage;

public interface TutorController {
    void handlePrimaryButtonPress();
    void handleSecondaryButtonPress();
    static TutorController newInstance(Territory territory, DefaultPage page) {
        return new TutorControllerImpl(territory, page);
    }
}