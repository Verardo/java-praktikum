package controller.tutor;

import model.Territory;
import tutor.RoleManager;
import tutor.message.Message;
import view.notification.Notification;
import view.page.DefaultPage;
import view.page.component.menu.MenuTutor;

import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;

class TutorControllerImpl implements TutorController {
    private final boolean isStudent;
    private final Territory territory;
    private final DefaultPage page;

    TutorControllerImpl(Territory territory, DefaultPage page) {
        this.isStudent = RoleManager.isStudent();
        this.territory = territory;
        this.page = page;
    }

    @Override
    public void handlePrimaryButtonPress() {
        MenuTutor menu = page.getMenu().getMenuTutor();

        // Disable button, so for a Tutor no next request can be loaded and for a student
        // no further requests can be sent
        menu.setDisableFirstItem(true);

        // If tutor: Get next request
        // If student: Send request
        if (isStudent) {
            try {
                RoleManager.sendRequestToTutor(page.getTextAreaContents(), territory.getAsXML());
                menu.setDisableSecondItem(false);
            } catch (XMLStreamException ex) {
                Notification.showErrorNotification("Territory could not be parsed to XML", ex.toString());
            }
        } else {
            Message req = null;
            try {
                req = RoleManager.getTutor().getNextRequest();
            } catch (RemoteException ex) {
                Notification.showErrorNotification("RMI Connection error", ex.toString());
                ex.printStackTrace();
            }
            if (req == null) {
                // Inform user
                Notification.showInfoNotification("No more requests",
                        "There are no more requests.");
                // Enable the primary button again
                menu.setDisableFirstItem(false);
            } else {
                page.setTextAreaContents(req.getCode());
                try {
                    territory.fromXML(req.getTerritory());
                } catch (XMLStreamException ex) {
                    Notification.showErrorNotification("Territory could not be parsed from XML", ex.toString());
                }
                menu.setDisableSecondItem(false);
            }
        }
    }

    @Override
    public void handleSecondaryButtonPress() {
        MenuTutor menu = page.getMenu().getMenuTutor();

        // Disable secondary button
        menu.setDisableSecondItem(true);

        // If tutor: Mark the request as answered
        // If student: Ask, whether an answer is present
        if (isStudent) {
            Message answer = RoleManager.getTutorAnswer();
            if (answer != null) {
                page.setTextAreaContents(answer.getCode());
                try {
                    territory.fromXML(answer.getTerritory());
                } catch (XMLStreamException ex) {
                    Notification.showErrorNotification("Territory could not be parsed from XML", ex.toString());
                }
                menu.setDisableFirstItem(false);
            } else {
                // Inform user
                Notification.showInfoNotification("No answer yet",
                        "The tutor hasn't answered yet :(");
                // Enable the secondary button again
                menu.setDisableSecondItem(false);
            }
        } else {
            try {
                RoleManager.getTutor().markCurrentAsAnswered(page.getTextAreaContents(), territory.getAsXML());
            } catch (XMLStreamException | RemoteException ex) {
                Notification.showErrorNotification("Could not mark as answered", ex.toString());
            }
            menu.setDisableFirstItem(false);
        }
    }
}