package controller.robot;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.AudioClip;
import model.Territory;

import java.io.File;

public class RobotDropControllerImpl implements RobotDropController, EventHandler<ActionEvent> {
    private final Territory territory;
    private final AudioClip clip = new AudioClip(new File("src/res/sound/error.wav").toURI().toString());

    RobotDropControllerImpl(Territory territory) {
        this.territory = territory;
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            territory.robotDrop();
        } catch (Exception ex) {
            clip.play();
        }
    }
}
