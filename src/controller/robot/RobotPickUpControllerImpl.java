package controller.robot;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.AudioClip;
import model.Territory;

import java.io.File;

class RobotPickUpControllerImpl implements RobotPickUpController, EventHandler<ActionEvent> {
    private final Territory territory;
    private final AudioClip clip = new AudioClip(new File("src/res/sound/error.wav").toURI().toString());

    RobotPickUpControllerImpl(Territory territory) {
        this.territory = territory;
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            territory.robotPickUp();
        } catch (Exception ex) {
            clip.play();
        }
    }
}