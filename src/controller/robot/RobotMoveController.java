package controller.robot;

import model.Territory;

public interface RobotMoveController {
    static RobotMoveController newInstance(Territory territory) {
        return new RobotMoveControllerImpl(territory);
    }
}
