package controller.robot;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.AudioClip;
import model.Territory;
import java.io.File;

class RobotMoveControllerImpl implements RobotMoveController, EventHandler<ActionEvent>{
    private final Territory territory;
    private final AudioClip clip = new AudioClip(new File("src/res/sound/error.wav").toURI().toString());

    RobotMoveControllerImpl(Territory territory) {
        this.territory = territory;
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            territory.robotMove();
        } catch (Exception ex) {
            clip.play();
        }
    }
}