package controller.robot;

import model.Territory;

public interface RobotPickUpController {
    static RobotPickUpController newInstance(Territory territory) {
        return new RobotPickUpControllerImpl(territory);
    }
}
