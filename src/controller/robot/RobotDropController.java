package controller.robot;

import model.Territory;

public interface RobotDropController {
    static RobotDropController newInstance(Territory territory) {
        return new RobotDropControllerImpl(territory);
    }
}
