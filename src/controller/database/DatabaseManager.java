package controller.database;

import model.Example;
import view.notification.Notification;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class DatabaseManager {
    private final static String DB_NAME = "exdb";
    public final static int NAME_LENGTH_LIMIT = 255;
    private final static int TAG_LENGTH_LIMIT = 255;
    private final static int CONNECTION_TIMEOUT = 1000;

    private final static String CREATE_TABLE_EXAMPLE = "CREATE TABLE Example ("
            + "name VARCHAR(" + String.valueOf(NAME_LENGTH_LIMIT) + ") PRIMARY KEY,"
            + "code LONG VARCHAR,"
            + "territory LONG VARCHAR) ";
    private final static String CREATE_TABLE_TAG = "CREATE TABLE Tag ("
            + "ex_name VARCHAR(" + NAME_LENGTH_LIMIT + ") REFERENCES Example(name),"
            + "tag VARCHAR(" + TAG_LENGTH_LIMIT + ")) ";
    private final static String INSERT_EXAMPLE = "INSERT INTO Example VALUES(?, ?, ?)";
    private final static String INSERT_TAG = "INSERT INTO Tag VALUES(?, ?)";

    private static Connection connection;

    public static void loadDBDriver() {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        } catch (ClassNotFoundException ex) {
            Notification.showErrorNotification("Database Driver Error",
                    "An error occured while trying to load the EmbeddedDriver.");
        }
    }

    public static void createDBIfNecessary() {
        if(!new File(DB_NAME).exists()) {
            try (Connection con = DriverManager.getConnection("jdbc:derby:" + DB_NAME + ";create=true");
                 Statement stmt = con.createStatement()) {
                stmt.execute(CREATE_TABLE_EXAMPLE);
                stmt.execute(CREATE_TABLE_TAG);
            } catch (SQLException ex) {
                Notification.showErrorNotification("DB Creation Error",
                        "There was an error trying to create the example database.\n"
                                + "Error: " + ex);
            }
        }
    }

    public static void connect() throws SQLException {
        connection = DriverManager.getConnection("jdbc:derby:" + DB_NAME + ";create=false");
    }

    public static synchronized boolean exampleExists(String name) {
        try {
            // Check if connection is still valid
            if(!connection.isValid(CONNECTION_TIMEOUT)) connect();

            // Query
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM Example WHERE name=?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();

            return rs.next();
        } catch (SQLException ex) {
            Notification.showErrorNotification("Database query error",
                    "There was an error while trying to execute a query.\n"
                            + "Error: " + ex);
            return true;
        }
    }

    static void insertExample(String name, String code, String territory, String[] tags) {
        boolean thrown = false;
        try {
            // Check if connection is still valid
            if(!connection.isValid(CONNECTION_TIMEOUT)) connect();

            // Alter auto commit
            connection.setAutoCommit(false);

            // Insert example
            PreparedStatement ps = connection.prepareStatement(INSERT_EXAMPLE);
            ps.setString(1, name);
            ps.setString(2, code);
            ps.setString(3, territory);
            ps.executeUpdate();

            // Insert tags
            for(String tag : tags) {
                if(tag.length() > TAG_LENGTH_LIMIT) throw new RuntimeException();
                ps = connection.prepareStatement(INSERT_TAG);
                ps.setString(1, name);
                ps.setString(2, tag);
                ps.executeUpdate();
            }

            connection.commit();
        } catch (SQLException ex) {
            Notification.showErrorNotification("Database Insertion Error",
                    "There was an error trying to insert a value into the database.\n"
                            + "Error: " + ex);
            thrown = true;
        } catch (RuntimeException ex) {
            Notification.showErrorNotification("Tag length over the limit!",
                    "The tag length must be between 1 and 255 characters.");
            thrown = true;
        } finally {
            try {
                if(connection != null) {
                    connection.setAutoCommit(true);
                    if(thrown) {
                        connection.rollback();
                    }
                }
            } catch (SQLException ex) {
                Notification.showErrorNotification("Database Connection Error",
                        "There was an error with the Database connection or during rollback.\n"
                                + "Error: " + ex);
            }
        }
    }

    static List<Example> getExamplesByTag(String tag) {
        List<Example> list = new ArrayList<>();
        try {
            // Check if connection is still valid
            if(!connection.isValid(CONNECTION_TIMEOUT)) connect();

            // Get all examples with the tag
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM Tag WHERE tag=?");
            ps.setString(1, tag);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                list.add(getExample(rs.getString("ex_name")));
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private static Example getExample(String name) {
        try {
            // Check if connection is still valid
            if(!connection.isValid(CONNECTION_TIMEOUT)) connect();

            // Query
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM Example WHERE name=?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();

            // No result?
            if(!rs.next()) return null;

            // Return result
            return Example.newInstance(name, rs.getString("code"), rs.getString("territory"));
        } catch (SQLException ex) {

            return null;
        }
    }
}
