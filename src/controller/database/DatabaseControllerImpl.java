package controller.database;

import javafx.util.Pair;
import model.Example;
import model.Territory;
import util.validator.Validator;
import view.dialog.Dialog;
import view.notification.Notification;
import view.page.DefaultPage;

import javax.xml.stream.XMLStreamException;
import java.util.List;

class DatabaseControllerImpl implements DatabaseController {
    private final Territory territory;
    private final DefaultPage page;

    DatabaseControllerImpl(Territory territory, DefaultPage page) {
        this.territory = territory;
        this.page = page;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void save() {
        Dialog dialog = Dialog.newDoubleInputDialog("Save in Database", "Save Example in Database",
                "Name:", "Tags (separate with\nblank spaces, minimum one):",
                "Name", "Tags");
        dialog.setValidatorFor(1, Validator.createValidatorDatabaseExampleName());
        dialog.setValidatorFor(2, Validator.createValidatorDatabaseTags());
        dialog.addObserver((o, arg) -> {
            Pair<String, String> pair = (Pair<String, String>)arg;
            try {
                DatabaseManager.insertExample(pair.getKey(), page.getTextAreaContents(), territory.getAsXML(),
                        pair.getValue().split(" "));
                Notification.showSuccessNotification("Successfully saved!",
                        "Your example was successfully saved!");
            } catch (XMLStreamException ex) {
                Notification.showErrorNotification("Failed to save example",
                        "There was an error trying to save the Example.\n"
                                + "Error: " + ex);
            }
        });
        dialog.showInputDialog();
    }

    @Override
    public void load() {
        Dialog dialog = Dialog.newSingleInputDialog("Load Example", "Load example",
                "Tag:", "Tag");
        dialog.setValidator(Validator.createValidatorNotEmptyString());
        dialog.addObserver((o, arg) -> {
            // Get examples with matching tag
            List<Example> examples = DatabaseManager.getExamplesByTag((String)arg);
            if(examples == null || examples.size() == 0) {
                Notification.showInfoNotification("No Example found",
                        "There was no example for this tag.");
                return;
            }

            // Get names as array
            String[] exNames = new String[examples.size()];
            for(int i = 0; i < exNames.length; i++) {
                exNames[i] = examples.get(i).getName();
            }

            Dialog selDialog = Dialog.newSelectionDialog("Select Example",
                    "Which Example would you like to open?", "Selected Example:", exNames);
            selDialog.addObserver((o2, arg2) -> {
                String name = (String)arg2;
                examples.forEach(ex -> {
                    if(ex.getName().equals(name)) {
                        page.setTextAreaContents(ex.getCode());
                        try {
                            territory.fromXML(ex.getTerritory());
                        } catch (XMLStreamException ex2) {
                            Notification.showErrorNotification("Failed to load Territory",
                                    "An error occured while trying to read the Territory from the Examle.\n" +
                                            "Error: The XML is corrupted.");
                        }
                    }
                });
            });
            selDialog.showInputDialog();
        });
        dialog.showInputDialog();
    }
}