package controller.database;

import model.Territory;
import view.page.DefaultPage;

public interface DatabaseController {
    void save();
    void load();
    static DatabaseController newInstance(Territory territory, DefaultPage page) {
        return new DatabaseControllerImpl(territory, page);
    }
}