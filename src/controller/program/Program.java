package controller.program;

public interface Program {
    void save(String contents);
    String load();
    String getName();
    static Program newInstance(String name) {
        return new ProgramImpl(name);
    }
}
