package controller.program;

import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.WindowEvent;
import model.Territory;
import view.Window;
import view.page.DefaultPage;
import view.page.Page;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class ProgramManager {
    public final static String FOLDER = "programs/";
    private final static String DEFAULT_PROGRAM_NAME = "DefaultRobot.java";
    private final static String TEMPLATE = "void main() {\n\n}";
    private final static List<Program> PROGRAMS = new ArrayList<>();
    private final static ProgramCompiler COMPILER = ProgramCompiler.newInstance();

    public static void run(String name) {
        run(name, Territory.newInstance());
    }

    public static void run(String name, Territory territory) {
        if(isRunning(name))
            new Alert(Alert.AlertType.ERROR, "This program is already running!", ButtonType.OK).showAndWait();
        else {
            // Create program and add to list
            Program prog = Program.newInstance(name);
            PROGRAMS.add(prog);

            // If program already exists, get it's contents,
            // otherwise: Set contents to TEMPLATE and save afterwards
            String progContent = prog.load();
            if(progContent == null) {
                progContent = TEMPLATE;
                prog.save(progContent);
            }

            // Try compilation
            COMPILER.compile(territory, name, false);

            // Launch window and handle closing event
            Window window = Window.newInstance(name);
            window.setPage(Page.newDefaultPage(territory, window, name, progContent));
            window.setOnClosingListener((EventHandler<WindowEvent>) e -> {
                if(window.getPage() instanceof DefaultPage) {
                    ((DefaultPage)window.getPage()).getBar().getSimulationManager().stop();
                }
                prog.save(((DefaultPage)window.getPage()).getTextAreaContents());
                PROGRAMS.remove(prog);
                if(PROGRAMS.isEmpty()) System.exit(0);
            });
            window.show();
        }
    }

    public static void runDefault() {
        run(DEFAULT_PROGRAM_NAME);
    }

    public static void runDefault(Territory territory) {
        run(DEFAULT_PROGRAM_NAME, territory);
    }

    public static void save(String programName, String contents) {
        PROGRAMS.forEach(prog -> {
            if(prog.getName().equals(programName)) prog.save(contents);
        });
    }

    public static boolean exists(String progName) {
        return new File(FOLDER + progName).exists();
    }

    public static ProgramCompiler getCompiler() {
        return COMPILER;
    }

    private static boolean isRunning(String name) {
        for(Program p : PROGRAMS) if(p.getName().equals(name)) return true;
        return false;
    }
}
