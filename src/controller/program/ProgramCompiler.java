package controller.program;

import model.Territory;

public interface ProgramCompiler {
    void compile(Territory territory, String progName, boolean showResult);
    static ProgramCompiler newInstance() {
        return new ProgramCompilerImpl();
    }
}