package controller.program;

import model.Robot;
import model.Territory;
import view.notification.Notification;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class ProgramCompilerImpl implements ProgramCompiler {
    @Override
    public void compile(Territory territory, String progName, boolean showResult) {
        // Compile
        JavaCompiler javac = ToolProvider.getSystemJavaCompiler();
        ByteArrayOutputStream errStream = new ByteArrayOutputStream();
        boolean success = javac.run(null, null, errStream, ProgramManager.FOLDER + progName) == 0;

        // If successfull, load the compiled class and update the
        // Robot instance in Territory
        if(success) {
            try{
                // Initialize class loader
                URLClassLoader classLoader = new URLClassLoader(new URL[] {
                        new File(ProgramManager.FOLDER).getAbsoluteFile().toURI().toURL()
                });

                // Load class and store a new instance in robot variable
                Robot robot = (Robot)classLoader.loadClass(progName.substring(0, progName.indexOf("."))).newInstance();

                // Use reflection to set the private territory variable in robot
                // (This way a setTerritory() method is not required)
                Field territoryField = robot.getClass().getSuperclass().getDeclaredField("territory");
                territoryField.setAccessible(true);
                territoryField.set(robot, territory);
                territoryField.setAccessible(false);

                // Update the robot instance in territory
                territory.setRobotInstance(robot);

            } catch (MalformedURLException ex) {
                Notification.showErrorNotification("ClassLoader error: Malformed URL", ex.toString());
                showResult = false;
            } catch (ClassNotFoundException ex) {
                Notification.showErrorNotification("ClassLoader error: Class not found", ex.toString());
                showResult = false;
            } catch (IllegalAccessException ex) {
                Notification.showErrorNotification("ClassLoader error: Illegal access", ex.toString());
                showResult = false;
            } catch (InstantiationException ex) {
                Notification.showErrorNotification("ClassLoader error: Instantiation failed", ex.toString());
                showResult = false;
            } catch (NoSuchFieldException ex) {
                Notification.showErrorNotification("ClassLoader error: Field \"territory\" not found", ex.toString());
                showResult = false;
            }
        }

        if(showResult) {
            if(success) Notification.showSuccessNotification("Compilation successfull!",
                    "The Compilation of \"" + progName + "\" was successfull!");
            else Notification.showErrorNotification("Compilation error:", errStream.toString());
        }
    }
}
