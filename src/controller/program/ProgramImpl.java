package controller.program;

import javafx.scene.control.Alert;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

class ProgramImpl implements Program {
    private final String name; // with .java
    private final String prefix;
    private final String postfix;

    ProgramImpl(String name) {
        this.name = name;
        prefix = "import controller.program.Invisible;\n\n"
                + "public class "
                + name.substring(0, name.indexOf(".")) // Name without .java
                + " extends model.RobotImpl implements model.Robot {\n"
                + "public ";
        postfix = "}";
    }

    @Override
    public void save(String contents) {
        new File(ProgramManager.FOLDER).mkdirs();
        try(FileWriter writer = new FileWriter(ProgramManager.FOLDER + name)) {
            writer.write(prefix + contents + postfix);
        } catch (IOException ex) {
            new Alert(Alert.AlertType.ERROR, "Error while trying to save the Program "
                    + "\"" + name + "\".").showAndWait();
            ex.printStackTrace();
        }
    }

    @Override
    public String load() {
        File file = new File(ProgramManager.FOLDER + name);
        if(!file.exists()) return null;
        try{
            String fileContent =  new String(Files.readAllBytes(Paths.get(ProgramManager.FOLDER + name)));
            return fileContent.substring(prefix.length(), fileContent.length() - postfix.length());
        } catch (IOException ex) {
            new Alert(Alert.AlertType.ERROR, "Error while trying to open the Program "
                    + "\"" + name + "\".").showAndWait();
            return null;
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
