package util.observer;

public interface Observer {
    void update(Observable obs, Object arg);
}