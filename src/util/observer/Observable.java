package util.observer;

import javafx.application.Platform;

import java.util.ArrayList;
import java.util.List;

public class Observable {
    private final List<Observer> obs = new ArrayList<>();

    public void addObserver(Observer o) {
        obs.add(o);
    }

    public void deleteObserver(Observer o) {
        obs.remove(o);
    }

    public void notifyObservers() {
        obs.forEach(o -> o.update(this, null));
    }

    public void notifyObservers(Object arg) {
        obs.forEach(o -> o.update(this, arg));
    }
}