package util.validator;

public class ValidatorNotEmptyString implements Validator {
    @Override
    public boolean validate(Object obj) {
        return obj instanceof String && ((String) obj).length() > 0;
    }

    @Override
    public String getErrorMessage() {
        return null;
    }
}
