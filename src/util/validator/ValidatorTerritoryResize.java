package util.validator;

import java.util.regex.Pattern;

public class ValidatorTerritoryResize implements Validator {
    private static final Pattern PATTERN = Pattern.compile("^[+-]?\\d+$");

    @Override
    public boolean validate(Object obj) {
        if(!(obj instanceof String)) return false;
        String str = (String)obj;
        return PATTERN.matcher(str).matches()
                && Integer.valueOf(str) > 0
                && Integer.valueOf(str) <= 100;
    }

    @Override
    public String getErrorMessage() {
        return "This value is either not a number or below zero/above 100";
    }
}