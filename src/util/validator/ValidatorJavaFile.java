package util.validator;

import controller.program.ProgramManager;

import java.util.regex.Pattern;

public class ValidatorJavaFile implements Validator {
    private static final Pattern PATTERN = Pattern.compile("^[A-Z]+[a-zA-Z0-9]*(\\.java)?");

    @Override
    public boolean validate(Object obj) {
        if(!(obj instanceof String)) return false;
        String str = (String)obj;
        return PATTERN.matcher(str).matches() && !ProgramManager.exists(str.endsWith(".java") ? str : str.concat(".java"));
    }

    @Override
    public String getErrorMessage() {
        return "Not a valid java file!";
    }
}