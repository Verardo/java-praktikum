package util.validator;

import java.util.regex.Pattern;

class ValidatorDatabaseTags implements Validator {
    private static final Pattern PATTERN = Pattern.compile("^[a-zA-Z0-9](([a-zA-Z0-9]| [a-zA-Z0-9]+)*)");

    @Override
    public boolean validate(Object obj) {
        return obj instanceof String && PATTERN.matcher((String)obj).matches();
    }

    @Override
    public String getErrorMessage() {
        return "Wrong format or spaces at the end of the line";
    }
}