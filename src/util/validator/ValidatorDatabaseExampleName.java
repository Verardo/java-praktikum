package util.validator;

import controller.database.DatabaseManager;

public class ValidatorDatabaseExampleName implements Validator {
    @Override
    public boolean validate(Object obj) {
        if(!(obj instanceof String)) return false;
        String str = (String)obj;
        return !DatabaseManager.exampleExists(str)
                && str.length() < DatabaseManager.NAME_LENGTH_LIMIT
                && str.length() > 0;
    }

    @Override
    public String getErrorMessage() {
        return "This name is already used!";
    }
}
