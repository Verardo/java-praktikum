package util.validator;

public interface Validator {
    /**
     * Validates an Object based on the validator's criteria.
     * @param obj The object to be validated
     * @return Returns true, if the Validator marks the given object as valid.
     */
    boolean validate(Object obj);

    /**
     * @return Returns the Validator's error message. Usually, the message is generic and not
     * detailed to the specific character/sentence that caused the error.
     */
    String getErrorMessage();

    /**
     * @return Returns a {@link ValidatorJavaFile}
     */
    static Validator createValidatorJavaFile() {
        return new ValidatorJavaFile();
    }

    /**
     * @return Returns a {@link ValidatorTerritoryResize}
     */
    static Validator createValidatorTerritoryResize() {
        return new ValidatorTerritoryResize();
    }

    /**
     * @return Returns a {@link ValidatorDatabaseTags}
     */
    static Validator createValidatorDatabaseTags() {
        return new ValidatorDatabaseTags();
    }

    /**
     * @return Returns a {@link ValidatorDatabaseExampleName}
     */
    static Validator createValidatorDatabaseExampleName() {
        return new ValidatorDatabaseExampleName();
    }

    /**
     * @return Returns a {@link ValidatorNotEmptyString}
     */
    static Validator createValidatorNotEmptyString() {
        return new ValidatorNotEmptyString();
    }
}