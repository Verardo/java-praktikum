package tutor;

import tutor.message.Message;
import tutor.message.MessageObjectBuilder;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

class TutorImpl extends UnicastRemoteObject implements Tutor {
    private int curId = 1;
    private final Map<Integer, Message> reqMap = new HashMap<>();
    private final Map<Integer, Message> ansMap = new HashMap<>();

    TutorImpl() throws RemoteException {
        super();
    }

    @Override
    public int sendRequest(Message req) throws RemoteException {
        int size = reqMap.size();
        reqMap.put(size + 1, req);
        return size + 1;
    }

    @Override
    public Message getAnswer(int id) throws RemoteException {
        return ansMap.get(id);
    }

    @Override
    public Message getNextRequest() throws RemoteException {
        // No more requests?
        if(curId > reqMap.size()) {
            return null;
        }

        // Return next request
        return reqMap.get(curId);
    }

    @Override
    public void markCurrentAsAnswered(String code, String territory) throws RemoteException {
        MessageObjectBuilder builder = MessageObjectBuilder.newInstance();
        builder.setCode(code);
        builder.setTerritory(territory);
        ansMap.put(curId, builder.build());
        curId++;
    }
}