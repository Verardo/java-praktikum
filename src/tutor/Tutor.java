package tutor;

import tutor.message.Message;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The student communicates to a Tutor via this interface (and RMI).
 */
public interface Tutor extends Remote {
    int sendRequest(Message req) throws RemoteException;
    Message getAnswer(int id) throws RemoteException;
    Message getNextRequest() throws RemoteException;
    void markCurrentAsAnswered(String code, String territory) throws RemoteException;
    static Tutor newInstance() throws RemoteException {
        return new TutorImpl();
    }
}