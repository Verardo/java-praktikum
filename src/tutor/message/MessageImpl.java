package tutor.message;

class MessageImpl implements Message {
    private final String code;
    private final String territory;

    MessageImpl(String code, String territory) {
        this.code = code;
        this.territory = territory;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getTerritory() {
        return territory;
    }
}