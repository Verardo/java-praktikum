package tutor.message;

public interface MessageObjectBuilder {
    void setCode(String code);
    void setTerritory(String territory);
    Message build();
    static MessageObjectBuilder newInstance() {
        return new MessageObjectBuilderImpl();
    }
}