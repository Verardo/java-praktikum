package tutor.message;

class MessageObjectBuilderImpl implements MessageObjectBuilder {
    private String code;
    private String territory;

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public void setTerritory(String territory) {
        this.territory = territory;
    }

    @Override
    public Message build() {
        return new MessageImpl(code, territory);
    }
}