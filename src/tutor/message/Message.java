package tutor.message;

import java.io.Serializable;

/**
 * Immutable message object
 */
public interface Message extends Serializable {
    String getCode();
    String getTerritory();
}