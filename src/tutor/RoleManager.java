package tutor;

import properties.PropertyManager;
import tutor.message.Message;
import tutor.message.MessageObjectBuilder;
import view.notification.Notification;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public abstract class RoleManager {
    private static boolean isStudent;
    private static int reqId;
    private static Tutor tutor;

    public static void loadRole() {
        isStudent = PropertyManager.getPropertyValue("role").equals("student");

        // If student, try to establish a connection, otherwise, build the server
        if(isStudent) {
            try {
                Registry reg = LocateRegistry.getRegistry(Integer.valueOf(PropertyManager.getPropertyValue(PropertyManager.PORT_KEY)));
                tutor = (Tutor)reg.lookup("Tutor");
            } catch (NotBoundException ex) {
                Notification.showErrorNotification("Tutor is not online",
                        "The tutor is currently not online. Please await, until your Tutor is online.");
            } catch (RemoteException ex) {
                Notification.showErrorNotification("RMI Connection Error", ex.toString());
            }
        } else {
            try {
                tutor = Tutor.newInstance();
                int port = Integer.valueOf(PropertyManager.getPropertyValue(PropertyManager.PORT_KEY));
                LocateRegistry.createRegistry(port);
                Registry reg = LocateRegistry.getRegistry(port);
                reg.rebind("Tutor", tutor);
            } catch (RemoteException ex) {
                Notification.showErrorNotification("Setting up Server failed!", ex.toString());
            }
        }
    }

    public static void sendRequestToTutor(String code, String terrtitory) {
        if(!isStudent) {
            throw new RuntimeException("You are a tutor!");
        }

        // Give code/territory data to a MessageObjectBuilder
        MessageObjectBuilder builder = MessageObjectBuilder.newInstance();
        builder.setCode(code);
        builder.setTerritory(terrtitory);

        // Send request
        try {
            reqId = tutor.sendRequest(builder.build());
        } catch (RemoteException ex) {
            Notification.showErrorNotification("RMI Connection Error", ex.toString());
        }
    }

    public static Message getTutorAnswer() {
        try {
            return tutor.getAnswer(reqId);
        } catch (RemoteException ex) {
            Notification.showErrorNotification("RMI Connection error", ex.toString());
            return null;
        }
    }

    public static boolean isStudent() {
        return isStudent;
    }

    public static Tutor getTutor() {
        return tutor;
    }
}