package view;

import view.page.Page;

import java.util.EventListener;

/**
 * The Window class represents one single window. An application may have several.
 */
public interface Window {
    /**
     * Sets the content of the window.
     * @param page Content of the window, expressed by a {@link Page} object.
     */
    void setPage(Page page);

    Page getPage();

    void setTitle(String title);

    void setOnClosingListener(Object listener);

    /**
     * Renders the window visible to the user.
     */
    void show();

    void close();

    static Window newInstance(String progName) {
        return new WindowImpl(progName);
    }

    static Window newInstance(Page page, String progName) {
        return new WindowImpl(page, progName);
    }
}
