package view.dialog;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import util.validator.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import java.util.Optional;

class SingleInputDialog extends Dialog<String> implements view.dialog.Dialog {
    private final TextField input = new TextField();
    private final Node okBtn;
    private final List<Observer> observer = new ArrayList<>();

    SingleInputDialog(String title, String headerText, String label, String promptTextInput) {
        // Title & Co
        setTitle(title);
        setHeaderText(headerText);

        // Set the button types
        ButtonType okBtnType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(okBtnType, ButtonType.CANCEL);

        // Create grid
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        // Set input text
        input.setPromptText(promptTextInput);

        // Add controls to grid
        grid.add(new Label(label), 0, 0);
        grid.add(input, 1,0);

        // Disable button
        okBtn = getDialogPane().lookupButton(okBtnType);
        okBtn.setDisable(true);

        // Add grid
        getDialogPane().setContent(grid);

        // Result
        setResultConverter(btn -> {
            if(!btn.getButtonData().isCancelButton()) return input.getText();
            else return null;
        });

        // Request focus on first text field
        Platform.runLater(input::requestFocus);
    }

    @Override
    public void setValidator(Validator validator) {
        input.textProperty().addListener((obs, oldValue, newValue) -> okBtn.setDisable(!validator.validate(newValue)));
    }

    @Override
    public void setValidatorFor(int inputIndex, Validator validator) {
        setValidator(validator);
    }

    @Override
    public void addObserver(Observer obs) {
        observer.add(obs);
    }

    @Override
    public void showInputDialog() {
        Optional<String> result = showAndWait();
        result.ifPresent(str -> observer.forEach((obs) -> obs.update(null, str)));
    }
}
