package view.dialog;

import util.validator.Validator;

import java.util.Observer;

public interface Dialog {
    void setValidator(Validator validator);
    void setValidatorFor(int inputIndex, Validator validator);
    void addObserver(Observer obs);
    void showInputDialog();

    static Dialog newSingleInputDialog(String title, String headerText, String label, String promptTextInput) {
        return new SingleInputDialog(title, headerText, label, promptTextInput);
    }

    static Dialog newDoubleInputDialog(String title, String headerText, String label1, String label2,
                                       String promptTextInput1, String promptTextInput2) {
        return new DoubleInputDialog(title, headerText, label1, label2, promptTextInput1, promptTextInput2);
    }

    static Dialog newDoubleInputDialog(String title, String headerText, String label1, String label2,
                                       String promptTextInput1, String promptTextInput2,
                                       String presetTextInput1, String presetTextInput2) {
        return new DoubleInputDialog(title, headerText, label1, label2, promptTextInput1, promptTextInput2,
                presetTextInput1, presetTextInput2);
    }

    static Dialog newSelectionDialog(String title, String headerText, String label, String[] options) {
        return new SelectionDialog(title, headerText, label, options);
    }
}
