package view.dialog;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import util.validator.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import java.util.Optional;

public class DoubleInputDialog extends Dialog<Pair<String, String>> implements view.dialog.Dialog {
    private final TextField input1 = new TextField();
    private final TextField input2 = new TextField();
    private final Node okBtn;
    private final List<Observer> observer = new ArrayList<>();
    private Validator validatorInput1;
    private Validator validatorInput2;

    DoubleInputDialog(String title, String headerText,
                      String label1, String label2, String promptTextInput1, String promptTextInput2) {
        // Title & Co
        setTitle(title);
        setHeaderText(headerText);

        // Set the button types
        ButtonType okBtnType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(okBtnType, ButtonType.CANCEL);

        // Create grid
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        // Set input text
        input1.setPromptText(promptTextInput1);
        input2.setPromptText(promptTextInput2);

        // Add controls to grid
        grid.add(new Label(label1), 0, 0);
        grid.add(input1, 1, 0);
        grid.add(new Label(label2), 0, 1);
        grid.add(input2, 1, 1);

        // Disable button
        okBtn = getDialogPane().lookupButton(okBtnType);
        okBtn.setDisable(true);

        // Add grid
        getDialogPane().setContent(grid);

        // Result
        setResultConverter(btn -> {
            if(!btn.getButtonData().isCancelButton())
                return new Pair<>(input1.getText(), input2.getText());
            else return null;
        });

        // Request focus on first text field
        Platform.runLater(input1::requestFocus);
    }

    DoubleInputDialog(String title, String headerText,
                      String label1, String label2, String promptTextInput1, String promptTextInput2,
                      String presetTextInput1, String presetTextInput2) {
        this(title, headerText, label1, label2, promptTextInput1, promptTextInput2);
        input1.setText(presetTextInput1);
        input2.setText(presetTextInput2);
    }

    @Override
    public void setValidator(Validator validator) {
        input1.textProperty().addListener((obs, oldValue, newValue)
                -> okBtn.setDisable(!validator.validate(newValue) || !validator.validate(input2.getText())));
        input2.textProperty().addListener((obs, oldValue, newValue)
                -> okBtn.setDisable(!validator.validate(newValue) || !validator.validate(input1.getText())));
    }

    @Override
    public void setValidatorFor(int inputIndex, Validator validator) {
        if(inputIndex < 1 || inputIndex > 2)
            throw new IllegalArgumentException("Input index must be greater than zero and smaller equal two!");

        if(inputIndex == 1 && validatorInput2 != null) {
            validatorInput1 = validator;
            input1.textProperty().addListener((obs, oldValue, newValue)
                    -> okBtn.setDisable(!validatorInput1.validate(newValue) || !validatorInput2.validate(input2.getText())));
            input2.textProperty().addListener((obs, oldValue, newValue)
                    -> okBtn.setDisable(!validatorInput1.validate(input1.getText()) || !validatorInput2.validate(newValue)));
        } else if(inputIndex == 1) {
            validatorInput1 = validator;
        } else if(validatorInput1 != null) {
            validatorInput2 = validator;
            input1.textProperty().addListener((obs, oldValue, newValue)
                    -> okBtn.setDisable(!validatorInput1.validate(newValue) || !validatorInput2.validate(input2.getText())));
            input2.textProperty().addListener((obs, oldValue, newValue)
                    -> okBtn.setDisable(!validatorInput1.validate(input1.getText()) || !validatorInput2.validate(newValue)));
        } else {
            validatorInput2 = validator;
        }
    }

    @Override
    public void addObserver(Observer obs) {
        observer.add(obs);
    }

    @Override
    public void showInputDialog() {
        Optional<Pair<String, String>> result = showAndWait();
        result.ifPresent(pair -> observer.forEach((obs) -> obs.update(null, pair)));
    }
}