package view.dialog;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import util.validator.Validator;

import java.util.*;

class SelectionDialog extends Dialog<String> implements view.dialog.Dialog {
    private final ChoiceBox<String> choiceBox = new ChoiceBox<>();
    private final Node okBtn;
    private final List<Observer> observers = new ArrayList<>();

    SelectionDialog(String title, String headerText, String label, String[] options) {
        // Title & Co
        setTitle(title);
        setHeaderText(headerText);

        // Set the button types
        ButtonType okBtnType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(okBtnType, ButtonType.CANCEL);

        // Create grid
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        // Fill the ChoiceBox and select first element if present
        Collections.addAll(choiceBox.getItems(), options);
        if(options.length > 0) choiceBox.getSelectionModel().select(options[0]);

        // Add controls to grid
        grid.add(new Label(label), 0, 0);
        grid.add(choiceBox, 1,0);

        // Get okBtn
        okBtn = getDialogPane().lookupButton(okBtnType);

        // Add grid
        getDialogPane().setContent(grid);

        // Result
        setResultConverter(btn -> {
            if(!btn.getButtonData().isCancelButton()) return choiceBox.getValue();
            else return null;
        });

        // Request focus on ChoiceBox
        Platform.runLater(choiceBox::requestFocus);
    }

    @Override
    public void setValidator(Validator validator) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setValidatorFor(int inputIndex, Validator validator) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addObserver(Observer obs) {
        observers.add(obs);
    }

    @Override
    public void showInputDialog() {
        Optional<String> result = showAndWait();
        result.ifPresent(str -> observers.forEach((obs) -> obs.update(null, str)));
    }
}