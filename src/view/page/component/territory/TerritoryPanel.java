package view.page.component.territory;

import model.Territory;
import model.Tile;
import view.page.DefaultPage;
import view.page.component.bar.Bar;

public interface TerritoryPanel {
    int CELL_SIZE = 32; // Size of a cell in pixels
    //int OUTER_GAP_IN_CELLS = 2; // Gap to border of the Region
    int LINE_SIZE = 1; // Size of the "border" between cells in pixels

    Tile getTileFromMousePosition(int x, int y);

    static TerritoryPanel newInstance(Territory territory, DefaultPage page, Bar bar, Object scrollPane) {
        return new TerritoryPanelImpl(territory, page, bar, scrollPane);
    }
}