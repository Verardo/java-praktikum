package view.page.component.territory;

import controller.territory.TerritoryPanelController;
import controller.territory.TerritoryPanelCursorController;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import model.Territory;
import model.Tile;
import util.observer.Observable;
import util.observer.Observer;
import view.page.DefaultPage;
import view.page.component.bar.Bar;

import java.util.HashMap;
import java.util.Map;

class TerritoryPanelImpl extends Region implements TerritoryPanel, Observer {
    private final Territory territory;
    private final Canvas canvas = new Canvas();
    private final TerritoryPanelController controller;
    private final TerritoryPanelCursorController cursorController;
    private final ScrollPane scrollPane;

    private final Map<Integer, Image> tileImg = new HashMap<>();
    private final Map<RobotImgKey, Image> robotImg = new HashMap<>();

    private enum RobotImgKey {
        ROBOT_NORTH,
        ROBOT_EAST,
        ROBOT_SOUTH,
        ROBOT_WEST,
        ROBOT_HUMAN_NORTH,
        ROBOT_HUMAN_EAST,
        ROBOT_HUMAN_SOUTH,
        ROBOT_HUMAN_WEST,
        ROBOT_GREEN_NORTH,
        ROBOT_GREEN_EAST,
        ROBOT_GREEN_SOUTH,
        ROBOT_GREEN_WEST
    }

    @SuppressWarnings("unchecked")
    TerritoryPanelImpl(Territory territory, DefaultPage page, Bar bar, Object scrollPane) {
        if(!(scrollPane instanceof ScrollPane))
            throw new IllegalArgumentException("The object has to be a javafx.scene.control.ScrollPane!");
        this.territory = territory;
        this.scrollPane = (ScrollPane)scrollPane;

        // Tile images
        tileImg.put(Territory.TILE_FIRE, new Image(getClass().getResourceAsStream("/res/game/fire.jpg")));
        tileImg.put(Territory.TILE_EMPTY, new Image(getClass().getResourceAsStream("/res/game/empty.png")));
        tileImg.put(Territory.TILE_HUMAN, new Image(getClass().getResourceAsStream("/res/game/human.png")));
        tileImg.put(Territory.TILE_RESCUE_ZONE, new Image(getClass().getResourceAsStream("/res/game/rescue_zone.png")));

        // Robot images
        robotImg.put(RobotImgKey.ROBOT_NORTH, new Image("res/game/robot_north.png"));
        robotImg.put(RobotImgKey.ROBOT_EAST, new Image("res/game/robot_east.png"));
        robotImg.put(RobotImgKey.ROBOT_SOUTH, new Image("res/game/robot_south.png"));
        robotImg.put(RobotImgKey.ROBOT_WEST, new Image("res/game/robot_west.png"));
        robotImg.put(RobotImgKey.ROBOT_HUMAN_NORTH, new Image("res/game/robot_human_north.png"));
        robotImg.put(RobotImgKey.ROBOT_HUMAN_EAST, new Image("res/game/robot_human_east.png"));
        robotImg.put(RobotImgKey.ROBOT_HUMAN_SOUTH, new Image("res/game/robot_human_south.png"));
        robotImg.put(RobotImgKey.ROBOT_HUMAN_WEST, new Image("res/game/robot_human_west.png"));
        robotImg.put(RobotImgKey.ROBOT_GREEN_NORTH, new Image("res/game/robot_green_north.png"));
        robotImg.put(RobotImgKey.ROBOT_GREEN_EAST, new Image("res/game/robot_green_east.png"));
        robotImg.put(RobotImgKey.ROBOT_GREEN_SOUTH, new Image("res/game/robot_green_south.png"));
        robotImg.put(RobotImgKey.ROBOT_GREEN_WEST, new Image("res/game/robot_green_west.png"));

        // Canvas
        canvas.setHeight((territory.getHeight()) * CELL_SIZE);
        canvas.setWidth((territory.getWidth()) * CELL_SIZE);

        // Center of ScrollPane
        ((ScrollPane) scrollPane).viewportBoundsProperty().addListener((obs, oldValue, newValue) -> {
            canvas.setLayoutX(newValue.getWidth() < canvas.getWidth() ? 0 : (newValue.getWidth() - canvas.getWidth()) / 2);
            canvas.setLayoutY(newValue.getHeight() < canvas.getHeight() ? 0 : (newValue.getHeight() - canvas.getHeight()) / 2);
        });

        // Mouse events
        controller = TerritoryPanelController.newInstance(territory, this, bar);
        cursorController = TerritoryPanelCursorController.newInstance(page);
        setOnMousePressed((EventHandler<MouseEvent>)controller);
        setOnMouseDragged((EventHandler<MouseEvent>)controller);
        setOnContextMenuRequested((EventHandler<ContextMenuEvent>)controller);
        canvas.setOnMouseEntered((EventHandler<MouseEvent>)cursorController);
        canvas.setOnMouseExited((EventHandler<MouseEvent>)cursorController);

        drawTerritory();
        this.getChildren().add(canvas);
    }

    private void drawTerritory() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setStroke(Color.DARKGRAY);
        gc.setLineWidth(LINE_SIZE);

        int height = territory.getHeight();
        int width = territory.getWidth();
        int robotRow = territory.getRobotRow();
        int robotCol = territory.getRobotCol();
        int robotDirection = territory.getRobotDirection();
        int canvasHeight = height * CELL_SIZE + height * LINE_SIZE;
        int canvasWidth = width * CELL_SIZE + width * LINE_SIZE;

        // Resize canvas
        canvas.setHeight(canvasHeight);
        canvas.setWidth(canvasWidth);

        // Center canvas
        canvas.setLayoutX(scrollPane.getWidth() < canvas.getWidth() ? 0 : (scrollPane.getWidth() - canvas.getWidth()) / 2);
        canvas.setLayoutY(scrollPane.getHeight() < canvas.getHeight() ? 0 : (scrollPane.getHeight() - canvas.getHeight()) / 2);

        // Clear canvas
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

        setPrefSize(canvasWidth, canvasHeight);

        // Draw territory
        for(int row = 0; row < height; row++ ) {
            for(int col = 0; col < width; col++) {
                int rowPX = row * CELL_SIZE + row * LINE_SIZE + 1;
                int colPX = col * CELL_SIZE + col * LINE_SIZE + 1;
                int tile = territory.getTile(row, col);

                // Draw borders
                gc.strokeLine(colPX, rowPX, colPX + CELL_SIZE, rowPX); // top
                gc.strokeLine(colPX, rowPX, colPX, rowPX + CELL_SIZE); // left
                if(row == height - 1) // bottom line
                    gc.strokeLine(colPX, rowPX + CELL_SIZE, colPX + CELL_SIZE, rowPX + CELL_SIZE);
                if(col == width - 1) // right line
                    gc.strokeLine(colPX + CELL_SIZE, rowPX, colPX + CELL_SIZE, rowPX + CELL_SIZE);

                // Draw tile content (Empty tile is drawn under every other tile
                if(tile == Territory.TILE_FIRE) {
                    gc.drawImage(tileImg.get(Territory.TILE_EMPTY), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                    gc.drawImage(tileImg.get(Territory.TILE_FIRE), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                } else if(tile == Territory.TILE_EMPTY) {
                    gc.drawImage(tileImg.get(Territory.TILE_EMPTY), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                } else if(tile == Territory.TILE_HUMAN) {
                    gc.drawImage(tileImg.get(Territory.TILE_EMPTY), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                    gc.drawImage(tileImg.get(Territory.TILE_HUMAN), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                } else if(tile == Territory.TILE_RESCUE_ZONE) {
                    gc.drawImage(tileImg.get(Territory.TILE_RESCUE_ZONE), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                } else throw new IllegalStateException("CRITICAL ERROR: Illegal Tile in [" + row + ", " + col +
                        "]. Error code: 0x02 - Illegal tile. Please report this bug immediately.");

                // Draw Robot if on this tile
                if(row == robotRow && col == robotCol) {
                    switch(robotDirection) {
                        case Territory.ROBOT_DIRECTION_NORTH:
                            if(territory.robotRescuing())
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_HUMAN_NORTH), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            else if(territory.robotRescueFinished())
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_GREEN_NORTH), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            else
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_NORTH), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            break;
                        case Territory.ROBOT_DIRECTION_EAST:
                            if(territory.robotRescuing())
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_HUMAN_EAST), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            else if(territory.robotRescueFinished())
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_GREEN_EAST), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            else
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_EAST), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            break;
                        case Territory.ROBOT_DIRECTION_SOUTH:
                            if(territory.robotRescuing())
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_HUMAN_SOUTH), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            else if(territory.robotRescueFinished())
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_GREEN_SOUTH), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            else
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_SOUTH), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            break;
                        case Territory.ROBOT_DIRECTION_WEST:
                            if(territory.robotRescuing())
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_HUMAN_WEST), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            else if(territory.robotRescueFinished())
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_GREEN_WEST), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            else
                                gc.drawImage(robotImg.get(RobotImgKey.ROBOT_WEST), colPX, rowPX, CELL_SIZE, CELL_SIZE);
                            break;
                        default:
                            throw new IllegalStateException("CRITICAL ERROR: The Robot is in an illegal state." +
                                    "Error Code: 0x01 - Robot direction unspecified. Please report this bug immediately.");
                    }
                }
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if(Platform.isFxApplicationThread()) drawTerritory();
        else Platform.runLater(this::drawTerritory);
    }

    @Override
    public Tile getTileFromMousePosition(int x, int y) {
        return Tile.newInstance(
                (y - (int)canvas.getLayoutY()) / CELL_SIZE,
                (x - (int)canvas.getLayoutX()) / CELL_SIZE
        );
    }
}
