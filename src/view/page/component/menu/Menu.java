package view.page.component.menu;

import model.Territory;
import view.Window;
import view.page.DefaultPage;

public interface Menu {
    MenuTerritory getMenuTerritory();
    MenuSimulation getMenuSimulation();
    MenuTutor getMenuTutor();
    static Menu newInstance(Territory territory, Window window, DefaultPage page, String progName) {
        return new MenuImpl(territory, window, page, progName);
    }
}