package view.page.component.menu;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import properties.Language;

class MenuSimulationImpl extends Menu implements MenuSimulation {
    private final MenuItem run = new MenuItem("_Run");
    private final MenuItem pause = new MenuItem("_Pause");
    private final MenuItem stop = new MenuItem("_Stop");

    MenuSimulationImpl() {
        super("_Simulation");
        Language.property().addListener((obs, oldValue, newValue)
                -> setText(Language.property().get().getString("simulation")));

        run.setAccelerator(KeyCombination.keyCombination("SHORTCUT+ALT+R"));
        run.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/run.png"))));

        pause.setAccelerator(KeyCombination.keyCombination("SHORTCUT+ALT+P"));
        pause.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/pause.png"))));

        stop.setAccelerator(KeyCombination.keyCombination("SHORTCUT+ALT+S"));
        stop.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/stop.png"))));

        getItems().addAll(run, pause, stop);
    }

    @Override
    public Object getMenuItemRun() {
        return run;
    }

    @Override
    public Object getMenuItemPause() {
        return pause;
    }

    @Override
    public Object getMenuItemStop() {
        return stop;
    }
}
