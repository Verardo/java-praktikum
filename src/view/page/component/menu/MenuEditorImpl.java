package view.page.component.menu;

import controller.editor.EditorFileNewController;
import controller.editor.EditorFileOpenController;
import controller.program.ProgramManager;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import model.Territory;
import properties.Language;
import view.Window;
import view.page.DefaultPage;

class MenuEditorImpl extends Menu implements MenuEditor {
    private final MenuItem fileNew = new MenuItem("_New");
    private final MenuItem fileOpen = new MenuItem("_Open");
    private final MenuItem save = new MenuItem("_Save");
    private final MenuItem compile = new MenuItem("_Compile");
    private final MenuItem print = new MenuItem("_Print");
    private final MenuItem quit = new MenuItem("_Quit");

    @SuppressWarnings("unchecked")
    MenuEditorImpl(Territory territory, Window window, String progName) {
        super("_Editor");
        Language.property().addListener((obs, oldValue, newValue)
                -> setText(Language.property().get().getString("editor")));

        fileNew.setAccelerator(KeyCombination.keyCombination("SHORTCUT+N"));
        fileNew.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/file_new.png"))));
        fileNew.setOnAction((EventHandler<ActionEvent>) EditorFileNewController.newInstance());

        fileOpen.setAccelerator(KeyCombination.keyCombination("SHORTCUT+O"));
        fileOpen.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/file_open.png"))));
        fileOpen.setOnAction((EventHandler<ActionEvent>) EditorFileOpenController.newInstance());

        save.setAccelerator(KeyCombination.keyCombination("SHORTCUT+S"));
        save.setOnAction(e -> ProgramManager.save(progName, ((DefaultPage)window.getPage()).getTextAreaContents()));

        compile.setAccelerator(KeyCombination.keyCombination("SHORTCUT+SHIFT+C"));
        compile.setOnAction(e -> {
            save.fire();
            ProgramManager.getCompiler().compile(territory, progName, true);
        });

        print.setAccelerator(KeyCombination.keyCombination("SHORTCUT+P"));
        print.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/print.png"))));
        print.setOnAction(e -> ((DefaultPage)window.getPage()).printTextArea());

        quit.setAccelerator(KeyCombination.keyCombination("SHORTCUT+Q"));
        quit.setOnAction(e -> window.close());

        getItems().addAll(fileNew, fileOpen, save, compile, print, quit);
    }
}