package view.page.component.menu;

import javafx.scene.control.MenuBar;
import model.Territory;
import view.Window;
import view.page.DefaultPage;

class MenuImpl extends MenuBar implements Menu {
    private final MenuEditor menuEditor;
    private final MenuTerritory menuTerritory;
    private final MenuRobot menuRobot;
    private final MenuSimulation menuSimulation = MenuSimulation.newInstance();
    private final MenuDatabase menuDatabase;
    private final MenuTutor menuTutor;
    private final MenuLanguage menuLanguage = MenuLanguage.newInstance();

    MenuImpl(Territory territory, Window window, DefaultPage page, String progName) {
        menuEditor = MenuEditor.newInstance(territory, window, progName);
        menuTerritory = MenuTerritory.newInstance(territory);
        menuRobot = MenuRobot.newInstance(territory);
        menuDatabase = MenuDatabase.newInstance(territory, page);
        menuTutor = MenuTutor.newInstance(territory, page);
        getMenus().addAll((javafx.scene.control.Menu)menuEditor,
                (javafx.scene.control.Menu)menuTerritory,
                (javafx.scene.control.Menu)menuRobot,
                (javafx.scene.control.Menu)menuSimulation,
                (javafx.scene.control.Menu) menuDatabase,
                (javafx.scene.control.Menu) menuTutor,
                (javafx.scene.control.Menu)menuLanguage);
    }

    @Override
    public MenuTerritory getMenuTerritory() {
        return menuTerritory;
    }

    @Override
    public MenuSimulation getMenuSimulation() {
        return menuSimulation;
    }

    @Override
    public MenuTutor getMenuTutor() {
        return menuTutor;
    }
}