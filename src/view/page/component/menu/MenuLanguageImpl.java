package view.page.component.menu;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import properties.Language;

import java.util.Locale;

class MenuLanguageImpl extends Menu implements MenuLanguage {
    private final MenuItem english = new MenuItem("English");
    private final MenuItem german = new MenuItem("German");
    private final MenuItem italian = new MenuItem("Italian");

    MenuLanguageImpl() {
        super("_Language");

        Language.property().addListener((obs, oldValue, newValue)
                -> setText(Language.property().get().getString("lang")));

        english.setOnAction((e) -> Language.setLocale(Locale.ENGLISH));
        english.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/english.png"))));

        german.setOnAction((e) -> Language.setLocale(Locale.GERMAN));
        german.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/german.png"))));

        italian.setOnAction((e) -> Language.setLocale(Locale.ITALIAN));
        italian.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/italian.png"))));

        getItems().addAll(english, german, italian);
    }
}