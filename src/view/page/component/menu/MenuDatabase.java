package view.page.component.menu;

import model.Territory;
import view.page.DefaultPage;

public interface MenuDatabase {
    static MenuDatabase newInstance(Territory territory, DefaultPage page) {
        return new MenuDatabaseImpl(territory, page);
    }
}