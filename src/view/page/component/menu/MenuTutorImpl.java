package view.page.component.menu;

import controller.tutor.TutorController;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.Territory;
import properties.Language;
import properties.PropertyManager;
import tutor.RoleManager;
import view.page.DefaultPage;

class MenuTutorImpl extends Menu implements MenuTutor {
    private final MenuItem firstItem;
    private final MenuItem secondItem;
    private final TutorController controller;

    MenuTutorImpl(Territory territory, DefaultPage page) {
        // Title of menu
        super("Tutor");
        Language.property().addListener((obs, oldValue, newValue)
                -> setText(Language.property().get().getString("tutor")));

        // Set items text depending on role
        if(RoleManager.isStudent()) {
            firstItem = new MenuItem("Send request");
            firstItem.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/send.png"))));
            secondItem = new MenuItem("Get feedback");
            secondItem.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/feedback.png"))));
        } else {
            firstItem = new MenuItem("Load request");
            secondItem = new MenuItem("Send feedback");
            secondItem.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/menu/send.png"))));
        }

        // Disable second item at start
        secondItem.setDisable(true);

        controller = TutorController.newInstance(territory, page);

        firstItem.setOnAction(e -> controller.handlePrimaryButtonPress());
        secondItem.setOnAction(e -> controller.handleSecondaryButtonPress());

        // Add to Menu
        this.getItems().addAll(firstItem, secondItem);
    }

    @Override
    public void setDisableFirstItem(boolean disable) {
        firstItem.setDisable(disable);
    }

    @Override
    public void setDisableSecondItem(boolean disable) {
        secondItem.setDisable(disable);
    }
}