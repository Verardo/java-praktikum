package view.page.component.menu;

import model.Territory;

public interface MenuTerritory {
    Object getMenuItemRobot();
    Object getMenuItemHuman();
    Object getMenuItemRescueZone();
    Object getMenuItemFire();
    Object getMenuItemClearTile();
    static MenuTerritory newInstance(Territory territory) {
        return new MenuTerritoryImpl(territory);
    }
}
