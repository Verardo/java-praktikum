package view.page.component.menu;

import controller.territory.TerritoryIOController;
import controller.territory.TerritoryResizeController;
import javafx.scene.control.*;
import javafx.scene.control.Menu;
import model.Territory;
import properties.Language;

class MenuTerritoryImpl extends Menu implements MenuTerritory {
    private final Menu save = new Menu("_Save As");
    private final MenuItem saveSerialize = new MenuItem("_Serialize (.territory)");
    private final MenuItem saveXml = new MenuItem("_XML (.xml)");

    private final Menu load = new Menu("_Load From");
    private final MenuItem loadDeserialize = new MenuItem("_Deserialize (.territory)");
    private final MenuItem loadXml = new MenuItem("_XML (.xml)");

    private final MenuItem print = new MenuItem("_Print");
    private final MenuItem resize = new MenuItem("_Resize");

    private final Menu export = new Menu("_Export as Image");
    private final MenuItem exportPng = new MenuItem("_Png file (.png)");
    private final MenuItem exportGif = new MenuItem("_Gif file (.gif)");

    private final ToggleGroup toggleGroup = new ToggleGroup();
    private final RadioMenuItem placeRobot = new RadioMenuItem("Place Robot");
    private final RadioMenuItem placeHuman = new RadioMenuItem("Place Human");
    private final RadioMenuItem placeRescueZone = new RadioMenuItem("Place RescueZone");
    private final RadioMenuItem placeFire = new RadioMenuItem("Place Fire");
    private final RadioMenuItem clearTile = new RadioMenuItem("Clear Tile");

    MenuTerritoryImpl(Territory territory) {
        super("_Territory");
        Language.property().addListener((obs, oldValue, newValue)
                -> setText(Language.property().get().getString("territory")));

        save.getItems().addAll(saveSerialize, saveXml);
        load.getItems().addAll(loadDeserialize, loadXml);
        export.getItems().addAll(exportPng, exportGif);

        resize.setOnAction(TerritoryResizeController.newInstance(territory));

        // Toggle group
        placeRobot.setToggleGroup(toggleGroup);
        placeHuman.setToggleGroup(toggleGroup);
        placeRescueZone.setToggleGroup(toggleGroup);
        placeFire.setToggleGroup(toggleGroup);
        clearTile.setToggleGroup(toggleGroup);
        placeRobot.setSelected(true);

        TerritoryIOController controller = TerritoryIOController.newInstance(territory);
        saveSerialize.setOnAction(e -> controller.saveSerialize());
        loadDeserialize.setOnAction(e -> controller.loadDeserialize());
        saveXml.setOnAction(e -> controller.saveXML());
        loadXml.setOnAction(e -> controller.loadXML());

        getItems().addAll(save, load, print, resize, export,
                new SeparatorMenuItem(), placeRobot, placeHuman, placeRescueZone, placeFire, clearTile);
    }

    @Override
    public Object getMenuItemRobot() {
        return placeRobot;
    }

    @Override
    public Object getMenuItemHuman() {
        return placeHuman;
    }

    @Override
    public Object getMenuItemRescueZone() {
        return placeRescueZone;
    }

    @Override
    public Object getMenuItemFire() {
        return placeFire;
    }

    @Override
    public Object getMenuItemClearTile() {
        return clearTile;
    }
}