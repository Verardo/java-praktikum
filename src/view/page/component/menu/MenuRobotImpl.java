package view.page.component.menu;

import controller.robot.RobotDropController;
import controller.robot.RobotMoveController;
import controller.robot.RobotPickUpController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import model.Territory;
import properties.Language;

class MenuRobotImpl extends Menu implements MenuRobot {
    private final MenuItem move = new MenuItem("_Move");
    private final MenuItem turn = new MenuItem("_Turn Right");
    private final MenuItem pickUp = new MenuItem("_Pick Up");
    private final MenuItem drop = new MenuItem("_Drop");

    @SuppressWarnings("unchecked")
    MenuRobotImpl(Territory territory) {
        super("_Robot");

        Language.property().addListener((obs, oldValue, newValue)
                -> setText(Language.property().get().getString("robot")));

        move.setAccelerator(KeyCombination.keyCombination("SHORTCUT+M"));
        move.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/robot_move.png"),
                16, 16, true, false)));
        move.setOnAction((EventHandler<ActionEvent>) RobotMoveController.newInstance(territory));

        turn.setAccelerator(KeyCombination.keyCombination("SHORTCUT+RIGHT"));
        turn.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/robot_turn.png"),
                16, 16, true, false)));
        turn.setOnAction(e -> territory.robotTurn());

        pickUp.setAccelerator(KeyCombination.keyCombination("SHORTCUT+UP"));
        pickUp.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/robot_pick_up.png"),
                16, 16, true, false)));
        pickUp.setOnAction((EventHandler<ActionEvent>) RobotPickUpController.newInstance(territory));

        drop.setAccelerator(KeyCombination.keyCombination("SHORTCUT+DOWN"));
        drop.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/robot_drop.png"),
                16, 16, true, false)));
        drop.setOnAction((EventHandler<ActionEvent>) RobotDropController.newInstance(territory));

        getItems().addAll(move, turn, pickUp, drop);
    }
}