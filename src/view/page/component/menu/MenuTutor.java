package view.page.component.menu;

import model.Territory;
import view.page.DefaultPage;

public interface MenuTutor {
    void setDisableFirstItem(boolean disable);
    void setDisableSecondItem(boolean disable);
    static MenuTutor newInstance(Territory territory, DefaultPage page) {
        return new MenuTutorImpl(territory, page);
    }
}