package view.page.component.menu;

public interface MenuSimulation {
    Object getMenuItemRun();
    Object getMenuItemPause();
    Object getMenuItemStop();
    static MenuSimulation newInstance() {
        return new MenuSimulationImpl();
    }
}
