package view.page.component.menu;

import controller.database.DatabaseController;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;
import model.Territory;
import properties.Language;
import view.page.DefaultPage;

class MenuDatabaseImpl extends Menu implements MenuDatabase {
    private final MenuItem save = new MenuItem("_Save Example");
    private final MenuItem load = new MenuItem("_Load Example");

    MenuDatabaseImpl(Territory territory, DefaultPage page) {
        super("_Database");
        Language.property().addListener((obs, oldValue, newValue)
                -> setText(Language.property().get().getString("database")));

        DatabaseController controller = DatabaseController.newInstance(territory, page);

        save.setAccelerator(KeyCombination.keyCombination("SHORTCUT+ALT+S"));
        save.setOnAction(e -> controller.save());

        load.setAccelerator(KeyCombination.keyCombination("SHORTCUT+ALT+L"));
        load.setOnAction(e -> controller.load());

        getItems().addAll(save, load);
    }
}