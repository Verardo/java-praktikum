package view.page.component.menu;

import model.Territory;

public interface MenuRobot {
    static MenuRobot newInstance(Territory territory) {
        return new MenuRobotImpl(territory);
    }
}
