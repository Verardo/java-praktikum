package view.page.component.menu;

public interface MenuLanguage {
    static MenuLanguage newInstance() {
        return new MenuLanguageImpl();
    }
}