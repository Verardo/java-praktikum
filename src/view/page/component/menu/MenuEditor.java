package view.page.component.menu;

import model.Territory;
import view.Window;

public interface MenuEditor {
    static MenuEditor newInstance(Territory territory, Window window, String progName) {
        return new MenuEditorImpl(territory, window, progName);
    }
}