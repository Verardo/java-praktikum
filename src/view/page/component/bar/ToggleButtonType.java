package view.page.component.bar;

public enum ToggleButtonType {
    ROBOT,
    HUMAN,
    RESCUE_ZONE,
    FIRE,
    CLEAR_TILE
}
