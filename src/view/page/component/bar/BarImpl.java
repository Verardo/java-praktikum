package view.page.component.bar;

import controller.editor.EditorFileNewController;
import controller.editor.EditorFileOpenController;
import controller.program.ProgramManager;
import controller.robot.RobotDropController;
import controller.robot.RobotMoveController;
import controller.robot.RobotPickUpController;
import controller.simulation.SimulationManager;
import controller.territory.TerritoryResizeController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import model.Territory;
import view.dialog.Dialog;
import view.page.DefaultPage;
import view.page.component.menu.Menu;
import view.page.component.menu.MenuSimulation;
import view.page.component.menu.MenuTerritory;

import java.util.Observable;
import java.util.Observer;

class BarImpl extends ToolBar implements Bar {
    // Basic controls
    private final Button btnFileNew = new Button();
    private final Button btnFileOpen = new Button();
    private final Button btnSave = new Button();
    private final Button btnCompile = new Button();

    // Territory controls
    private final Button btnTerritoryResize = new Button();
    private final ToggleGroup territoryToggleGroup = new ToggleGroup();
    private final ToggleButton btnPlaceRobot = new ToggleButton();
    private final ToggleButton btnPlaceHuman = new ToggleButton();
    private final ToggleButton btnPlaceRescueZone = new ToggleButton();
    private final ToggleButton btnPlaceFire = new ToggleButton();
    private final ToggleButton btnClearTile = new ToggleButton();

    // Robot action controls
    private final Button btnRobotMove = new Button();
    private final Button btnRobotTurn = new Button();
    private final Button btnRobotPickUp = new Button();
    private final Button btnRobotDrop = new Button();

    // Simulation controls
    private final Button btnRun = new Button();
    private final Button btnPause = new Button();
    private final Button btnStop = new Button();
    private final Slider slider = new Slider();

    // Objects needed for Event processing
    private final Territory territory;
    private final String progName;
    private final DefaultPage page;
    private final SimulationManager SIM_MGR;

    BarImpl(Territory territory, DefaultPage page, Menu menu, String progName) {
        this.territory = territory;
        this.page = page;
        this.progName = progName;
        SIM_MGR = SimulationManager.newInstance(territory);

        // Initialize all GUI elements
        initializeBasicControls();
        initializeTerritoryControls();
        initializeRobotActionControls();
        initializeSimulationControls();

        // Bind "place x" and "clear tile" elements to those of the MenuTerritory
        MenuTerritory menuTerritory = menu.getMenuTerritory();
        btnPlaceRobot.selectedProperty().bindBidirectional(((RadioMenuItem)menuTerritory.getMenuItemRobot()).selectedProperty());
        btnPlaceHuman.selectedProperty().bindBidirectional(((RadioMenuItem)menuTerritory.getMenuItemHuman()).selectedProperty());
        btnPlaceRescueZone.selectedProperty().bindBidirectional(((RadioMenuItem)menuTerritory.getMenuItemRescueZone()).selectedProperty());
        btnPlaceFire.selectedProperty().bindBidirectional(((RadioMenuItem)menuTerritory.getMenuItemFire()).selectedProperty());
        btnClearTile.selectedProperty().bindBidirectional(((RadioMenuItem)menuTerritory.getMenuItemClearTile()).selectedProperty());

        // Bind simulation buttons
        MenuSimulation menuSimulation = menu.getMenuSimulation();
        MenuItem menuSimulationRun = (MenuItem)menuSimulation.getMenuItemRun();
        MenuItem menuSimulationPause = (MenuItem)menuSimulation.getMenuItemPause();
        MenuItem menuSimulationStop = (MenuItem)menuSimulation.getMenuItemStop();

        menuSimulationRun.disableProperty().bind(btnRun.disableProperty());
        menuSimulationPause.disableProperty().bind(btnPause.disableProperty());
        menuSimulationStop.disableProperty().bind(btnStop.disableProperty());

        menuSimulationRun.setOnAction(e -> btnRun.fire());
        menuSimulationPause.setOnAction(e -> btnPause.fire());
        menuSimulationStop.setOnAction(e -> btnStop.fire());

        // Add all elements
        getItems().addAll(btnFileNew, btnFileOpen, btnSave, btnCompile,
                new Separator(), btnTerritoryResize,
                new Separator(), btnPlaceRobot, btnPlaceHuman, btnPlaceRescueZone, btnPlaceFire, btnClearTile,
                new Separator(), btnRobotMove, btnRobotTurn, btnRobotPickUp, btnRobotDrop,
                new Separator(), btnRun, btnPause, btnStop, new Separator(), slider);
    }

    @SuppressWarnings("unchecked")
    private void initializeBasicControls() {
        btnFileNew.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/file_new.png"))));
        btnFileNew.setTooltip(new Tooltip("Create a new program"));
        btnFileNew.setOnAction((EventHandler<ActionEvent>) EditorFileNewController.newInstance());

        btnFileOpen.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/file_open.png"))));
        btnFileOpen.setTooltip(new Tooltip("Open a program file"));
        btnFileOpen.setOnAction((EventHandler<ActionEvent>) EditorFileOpenController.newInstance());

        btnSave.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/save.png"))));
        btnSave.setTooltip(new Tooltip("Save the current program"));
        btnSave.setOnAction(e -> ProgramManager.save(progName, page.getTextAreaContents()));

        btnCompile.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/compile.png"))));
        btnCompile.setTooltip(new Tooltip("Compile the current program"));
        btnCompile.setOnAction(e -> {
            btnSave.fire();
            ProgramManager.getCompiler().compile(territory, progName, true);
        });
    }

    @SuppressWarnings("unchecked")
    private void initializeTerritoryControls() {
        btnTerritoryResize.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/grid.png"))));
        btnTerritoryResize.setTooltip(new Tooltip("Resize the territory"));
        btnTerritoryResize.setOnAction(TerritoryResizeController.newInstance(territory));

        btnPlaceRobot.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/robot.png"))));
        btnPlaceRobot.setTooltip(new Tooltip("Move the Robot"));
        btnPlaceRobot.setToggleGroup(territoryToggleGroup);
        btnPlaceRobot.setSelected(true);

        btnPlaceHuman.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/human.png"))));
        btnPlaceHuman.setTooltip(new Tooltip("Place a human"));
        btnPlaceHuman.setToggleGroup(territoryToggleGroup);

        btnPlaceRescueZone.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/rescue_zone.png"))));
        btnPlaceRescueZone.setTooltip(new Tooltip("Place a rescue zone"));
        btnPlaceRescueZone.setToggleGroup(territoryToggleGroup);

        btnPlaceFire.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/fire.png"))));
        btnPlaceFire.setTooltip(new Tooltip("Place a fire"));
        btnPlaceFire.setToggleGroup(territoryToggleGroup);

        btnClearTile.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/clear.png"))));
        btnClearTile.setTooltip(new Tooltip("Clear a tile (removes all it's contents)"));
        btnClearTile.setToggleGroup(territoryToggleGroup);
    }

    @SuppressWarnings("unchecked")
    private void initializeRobotActionControls() {
        btnRobotMove.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/robot_move.png"))));
        btnRobotMove.setTooltip(new Tooltip("Move the robot one tile in the direction that it's currently facing"));
        btnRobotMove.setOnAction((EventHandler<ActionEvent>) RobotMoveController.newInstance(territory));

        btnRobotTurn.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/robot_turn.png"))));
        btnRobotTurn.setTooltip(new Tooltip("Rotate the robot to the right"));
        btnRobotTurn.setOnAction(e -> territory.robotTurn());

        btnRobotPickUp.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/robot_pick_up.png"))));
        btnRobotPickUp.setTooltip(new Tooltip("Pick up a human"));
        btnRobotPickUp.setOnAction((EventHandler<ActionEvent>) RobotPickUpController.newInstance(territory));

        btnRobotDrop.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/robot_drop.png"))));
        btnRobotDrop.setTooltip(new Tooltip("Drop a human"));
        btnRobotDrop.setOnAction((EventHandler<ActionEvent>) RobotDropController.newInstance(territory));
    }

    private void initializeSimulationControls() {
        btnRun.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/run.png"))));
        btnRun.setTooltip(new Tooltip("Start/Continue the simulation"));
        btnRun.setOnAction(e -> {
            btnRun.setDisable(true);
            btnPause.setDisable(false);
            btnStop.setDisable(false);
            SIM_MGR.run(territory);
        });

        btnPause.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/pause.png"))));
        btnPause.setTooltip(new Tooltip("Pause the simulation"));
        btnPause.setDisable(true);
        btnPause.setOnAction(e -> {
            btnRun.setDisable(false);
            btnPause.setDisable(true);
            btnStop.setDisable(false);
            SIM_MGR.pause();
        });

        btnStop.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/bar/stop.png"))));
        btnStop.setTooltip(new Tooltip("Stop the simulation"));
        btnStop.setDisable(true);
        btnStop.setOnAction(e -> {
            btnRun.setDisable(false);
            btnPause.setDisable(true);
            btnStop.setDisable(true);
            SIM_MGR.stop();
        });
        SIM_MGR.addObserver((obs, arg) -> Platform.runLater(() -> {
            btnRun.setDisable(false);
            btnPause.setDisable(true);
            btnStop.setDisable(true);
        }));

        slider.setMin(1);
        slider.setMax(10);
        slider.setValue(5);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.setMajorTickUnit(1);
        slider.setMinorTickCount(0);
        slider.setSnapToTicks(true);
        slider.setPrefWidth(200);
        slider.setTooltip(new Tooltip("Adjust the speed of the simulation. " +
                "1 is very slow, whereas 10 is very fast. The default value is 5."));
        slider.valueProperty().addListener((obs, oldValue, newValue) -> SIM_MGR.setSpeed((int)slider.getValue()));
    }

    @Override
    public ToggleButtonType selectedToggleButton() {
        if(btnPlaceRobot.isSelected()) return ToggleButtonType.ROBOT;
        else if(btnPlaceHuman.isSelected()) return ToggleButtonType.HUMAN;
        else if(btnPlaceRescueZone.isSelected()) return ToggleButtonType.RESCUE_ZONE;
        else if(btnPlaceFire.isSelected()) return ToggleButtonType.FIRE;
        else if(btnClearTile.isSelected()) return ToggleButtonType.CLEAR_TILE;
        else return null;
    }

    @Override
    public SimulationManager getSimulationManager() {
        return SIM_MGR;
    }
}