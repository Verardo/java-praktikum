package view.page.component.bar;

import controller.simulation.SimulationManager;
import model.Territory;
import view.page.DefaultPage;
import view.page.component.menu.Menu;
import view.page.component.menu.MenuTerritory;

public interface Bar {
    /**
     * @return Returns the currently selected Button in the ToggleGroup.
     * The options are specified by {@link ToggleButtonType}.
     */
    ToggleButtonType selectedToggleButton();

    SimulationManager getSimulationManager();

    /**
     * Acts as a factory.
     * @return Returns a new instance of the Bar interface.
     */
    static Bar newInstance(Territory territory, DefaultPage page, Menu menu, String progName) {
        return new BarImpl(territory, page, menu, progName);
    }
}
