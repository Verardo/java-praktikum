package view.page;

import javafx.application.Platform;
import javafx.print.PrinterJob;
import javafx.scene.ImageCursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import model.Territory;
import util.observer.Observer;
import view.Window;
import view.notification.Notification;
import view.page.component.bar.Bar;
import view.page.component.menu.Menu;
import view.page.component.territory.TerritoryPanel;

import javax.swing.text.html.ImageView;

class DefaultPageImpl extends BorderPane implements DefaultPage {
    private final Scene scene;
    private final Menu menu;
    private final Bar bar;
    private final TextArea textArea = new TextArea();
    private final TerritoryPanel territoryPanel;

    DefaultPageImpl(Territory territory, Window window, String progName) {
        // Initialize bar and menu
        menu = Menu.newInstance(territory, window, this, progName);
        bar = Bar.newInstance(territory, this, menu, progName);

        setTop((Node)menu);

        ScrollPane scrollPane = new ScrollPane();
        territoryPanel = TerritoryPanel.newInstance(territory, this, bar, scrollPane);
        scrollPane.setContent((Region)territoryPanel);

        territory.addObserver((Observer)territoryPanel);

        SplitPane splitPane = new SplitPane();
        splitPane.getItems().addAll(textArea, scrollPane);

        BorderPane inner = new BorderPane();
        inner.setTop((Node)bar);
        inner.setCenter(splitPane);

        setCenter(inner);
        scene = new Scene(this, 1281, 723);
    }

    DefaultPageImpl(Territory territory, Window window, String progName, String textAreaContents) {
        this(territory, window, progName);
        textArea.setText(textAreaContents);
    }

    @Override
    public Object getRootObject() {
        return scene;
    }

    @Override
    public void printTextArea() {
        new Thread(() -> {
            PrinterJob job = PrinterJob.createPrinterJob();
            if(job != null) {
                boolean success = job.printPage(textArea);
                if (success) {
                    job.endJob();
                    Platform.runLater(() -> Notification.showSuccessNotification("Printing done!",
                            "Printed the program successfully!"));
                } else
                    Platform.runLater(() -> Notification.showErrorNotification("Printing failed! :(",
                            "Printing of the program failed."));
            } else Platform.runLater(() -> Notification.showErrorNotification("No Printer found :(",
                    "Printing of the program failed, as no printer could be found on your system."));
        }).start();
    }

    @Override
    public String getTextAreaContents() {
        return textArea.getText();
    }

    @Override
    public void setTextAreaContents(String str) {
        textArea.setText(str);
    }

    @Override
    public Bar getBar() {
        return bar;
    }

    @Override
    public Menu getMenu() {
        return menu;
    }
}
