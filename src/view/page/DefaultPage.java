package view.page;

import view.page.component.bar.Bar;
import view.page.component.menu.Menu;

public interface DefaultPage extends Page {
    void printTextArea();
    String getTextAreaContents();
    void setTextAreaContents(String str);
    Bar getBar();
    Menu getMenu();
}
