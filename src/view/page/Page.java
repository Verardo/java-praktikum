package view.page;

import model.Territory;
import view.Window;

public interface Page {
    /**
     * @return Returns the root object of the page. A root object contains all visual elements.
     * When calling this method make sure that the returning object is the expected one.
     */
    Object getRootObject();

    /**
     * Acts as a factory.
     * @return Returns a new instance of the default page.
     */
    static DefaultPage newDefaultPage(Territory territory, Window window, String progName) {
        return new DefaultPageImpl(territory, window, progName);
    }

    static DefaultPage newDefaultPage(Territory territory, Window window, String progName, String textAreaContents) {
        return new DefaultPageImpl(territory, window, progName, textAreaContents);
    }
}
