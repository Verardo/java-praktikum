package view.notification;

import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

class InfoNotification extends Dialog implements Notification {
    InfoNotification(String headerText, String contentText) {
        setTitle("Information");
        setHeaderText(headerText);
        getDialogPane().setContent(new Label(contentText));
        setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/notification/info.png"))));
        getDialogPane().getButtonTypes().add(new ButtonType("OK", ButtonBar.ButtonData.OK_DONE));
        showAndWait();
    }
}