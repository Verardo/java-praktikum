package view.notification;

public interface Notification {
    static void showSuccessNotification(String headerText, String contentText) {
        new SuccessNotification(headerText, contentText);
    }

    static void showErrorNotification(String headerText, String contentText) {
        new ErrorNotification(headerText, contentText);
    }

    static void showInfoNotification(String headerText, String contentText) {
        new InfoNotification(headerText, contentText);
    }
}
