package view.notification;

import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

class ErrorNotification extends Dialog implements Notification {
    ErrorNotification(String headerText, String contentText) {
        setTitle("Error!");
        setHeaderText(headerText);
        getDialogPane().setContent(new Label(contentText));
        setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/notification/error.png"))));
        getDialogPane().getButtonTypes().add(new ButtonType("OK", ButtonBar.ButtonData.OK_DONE));
        showAndWait();
    }
}