package view.notification;

import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

class SuccessNotification extends Dialog implements Notification {
    SuccessNotification(String headerText, String contentText) {
        setTitle("Success!");
        setHeaderText(headerText);
        getDialogPane().setContent(new Label(contentText));
        setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/res/notification/success.png"))));
        getDialogPane().getButtonTypes().add(new ButtonType("OK", ButtonBar.ButtonData.OK_DONE));
        showAndWait();
    }
}
