package view;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import view.page.Page;

class WindowImpl implements Window {
    private final Stage stage = new Stage();
    private Page page;
    private final static String TITLE_POSTFIX = " - Rescue Robot 1.12 Final";

    WindowImpl(String progName) {
        // My icon is blurry, so I tried to add all regular icon sizes, as apparently
        // Java and the OS (in my case elementary OS) choose the "optimal" icon size.
        // However it's still blurry, sadly
        // Reference: https://stackoverflow.com/questions/29356066/low-quality-icon-in-taskbar-of-a-stage-javafx#29358121
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/res/logo_32.png")));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/res/logo_48.png")));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/res/logo_64.png")));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/res/logo_96.png")));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/res/logo_128.png")));
        stage.setTitle(progName + TITLE_POSTFIX);
    }

    WindowImpl(Page page, String progName) {
        this(progName);
        this.page = page;
        stage.setScene((Scene)page.getRootObject());
    }

    @Override
    public void setPage(Page page) {
        this.page = page;
        stage.setScene((Scene)page.getRootObject());
    }

    @Override
    public Page getPage() {
        return page;
    }

    @Override
    public void setTitle(String title) {
        stage.setTitle(title);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setOnClosingListener(Object listener) {
        stage.setOnCloseRequest((EventHandler<WindowEvent>)listener);
    }

    @Override
    public void show() {
        stage.show();
    }

    @Override
    public void close() {
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }
}
