package properties;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class Language {
    private static final ObjectProperty<ResourceBundle> BUNDLE = new SimpleObjectProperty<>();
    private static final Map<String, Locale> MAP = new HashMap<>();

    public static void setLocale(Locale loc) {
        BUNDLE.set(ResourceBundle.getBundle("language", loc));
    }

    public static ObjectProperty<ResourceBundle> property() {
        return BUNDLE;
    }

    public static void load() {
        // Fill map with all known values
        MAP.put("en", Locale.ENGLISH);
        MAP.put("de", Locale.GERMAN);
        MAP.put("it", Locale.ITALIAN);

        // Load locale
        Locale loc = MAP.get(PropertyManager.getPropertyValue(PropertyManager.LANG_KEY).toLowerCase());
        if(loc != null) {
            setLocale(loc);
        } else { /* Default locale */
            setLocale(Locale.GERMAN);
        }
    }
}