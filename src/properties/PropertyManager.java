package properties;

import java.io.*;
import java.util.Locale;
import java.util.Properties;

public abstract class PropertyManager {
    private final static String fileName = "sim.config";
    private final static String hint = "#role=tutor or student";
    private static Properties props;

    // Keys
    public final static String ROLE_KEY = "role";
    public final static String HOST_KEY = "tutorhost";
    public final static String PORT_KEY = "tutorport";
    public final static String LANG_KEY = "language";

    // Default values
    private final static String roleDefaultValue = "tutor";
    private final static String tutorHostDefaultValue = "localhost";
    private final static String tutorPortDefaultValue = "3579";
    private final static String languageDefaultValue = "de";

    public static void loadProperties() {
        // Check: Does properties file exist?
        if(!new File(fileName).exists()) {
            // Create file and write default values
            try (FileOutputStream fileOut = new FileOutputStream(fileName);
                PrintStream ps = new PrintStream(fileOut)) {
                ps.print(hint + "\n");
                ps.print(ROLE_KEY + "=" + roleDefaultValue + "\n");
                ps.print(HOST_KEY + "=" + tutorHostDefaultValue + "\n");
                ps.print(PORT_KEY + "=" + tutorPortDefaultValue + "\n");
                ps.print(LANG_KEY + "=" + languageDefaultValue);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        try (FileReader reader = new FileReader(fileName)) {
            System.getProperties().load(reader);
            props = System.getProperties();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static String getPropertyValue(String key) {
        return props.getProperty(key);
    }
}