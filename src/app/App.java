package app;

import controller.database.DatabaseManager;
import controller.program.ProgramManager;
import javafx.application.Application;
import javafx.stage.Stage;
import properties.Language;
import properties.PropertyManager;
import tutor.RoleManager;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        PropertyManager.loadProperties();
        RoleManager.loadRole();
        DatabaseManager.loadDBDriver();
        DatabaseManager.createDBIfNecessary();
        DatabaseManager.connect(); // Comment out if testing RMI
        ProgramManager.runDefault();
        Language.load();
    }
}