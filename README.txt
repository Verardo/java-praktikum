Hallo Dibo!

Hier noch einige Hinweise zu meiner Abgabe:

(1) Entwickelt wurde das Programm in IntelliJ IDEA CE, es ist allerdings als Eclipse-Projekt exportiert und
    sollte damit (hoffentlich) problemlos in Eclipse gestartet werden können.

(2) Das Projekt sitzt auf einem privaten git-repsitory (bitbucket.org). Es sollte ohne weiteres funktionieren
    und auch keine Probleme machen, aber unter IntelliJ wird z.B. eine Info-Nachricht angezeigt.
    Falls es aus irgendeinem Grund nicht gehen sollte, das Projekt ohne git zu starten, sind dies (bis zum 28.
    Februar 2018) die Anmeldedaten:
    
    Webseite: https://bitbucket.org
    (Username: Verardo)
    E-Mail: verardo@protonmail.com
    Passwort: javadibo123
	
(3) Ich habe das Projekt von "java-praktikum" nach "TomasPenalverVerardoSimuator" umbenant, wie erfordert.
    Dies wird mir an manchen Stellen auch angezeigt, an anderen aber widerrum nicht.
    Deswegen weiß ich leider nicht, ob die Umbennung vollständig geklappt hat.
    
(4) In der Datei "Rescue_Robot.pdf" ist die Miniprogrammierwelt beschrieben.
    Dazu folgende Hinweise: SafeZones wurden in RescueZones umbenannt, statt turnLeft() wurde
    im Programm turnRight() implementiert. Weiterhin (steht nur indirekt in der PDF): 
	RescueZones sollen die Menschen "retten", d.h. aus der Gefahrenzone (und damit von der Karte) 
	entfernen.
	
(5) Besonderheiten
(5.1) Zur Visualisierung ist der Roboter grün gefärbt, wenn alle Menschen gerettet wurden bzw.
	  es keine Menschen mehr auf der Karte gibt, ansonsten blau.
(5.2) Bei der Auswahl des Kachel-Werkzeuges (d.h. wenn man z.B. eine Feuer-Kachel platzieren möchte)
	  ändert sich der Cursor zu einem Kleinformat des zu platzierenden Objekts. Die Idee dazu habe ich
	  von Maik Fischer. Die Implementierung ist jedoch von mir.
(5.3) Wenn man beim Platzieren von Elementen auf einer Kachel, mit Ausnahme des Roboters, oder beim
	  Löschen einer Kachel die Maus gedrückt hält, wird das Platzieren/Löschen solange wie die Maus
	  gedrückt ist ausgeführt. Dies erlaubt schnelleres Platzieren der z.B. Menschen.
(5.4) Beim Verkleinern des Feldes wird der Roboter am ersten freien Platz platziert.
	  Nur wenn es den nicht gibt, wird das Feuer an der Kachel (0,0) entfernt und der Roboter
	  dort platziert.

(6) Funktionalität
	Ich habe alle nicht-optionalen Aufgaben implementiert und von den optionalen eine der
	Druck-Aufgaben (Code drucken, wobei dies nicht getestet ist) und die optionale Aufgabe von
	Blatt 2.
	Beim letzten Zettel (Internationalisierung) habe ich aus Zeitgründen nur das Menü internationalisiert.
	
So, das wars auch schon. Viele Grüße & viel Spaß beim Testen :)
